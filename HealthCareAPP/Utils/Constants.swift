//
//  Constants.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 21/11/22.
//

import SwiftUI

struct Constants {
    
    static let specialistArray = ["Anesthesiologists", "Cardiologists", "Dermatologists", "Endocrinologists", "Gastroenterologists", "Hematologists", "Internists", "Neurologists", "Pediatricians", "Urologists", "General Physicians"]
    static let genderArray = [Constants.male, Constants.female]
    static let registeredAsArray = [Constants.doctor, Constants.patient]
    
    
    //MARK - Color Constant
    static  let primaryColor : Color = CommonFunctionUtils.hexStringToUIColor(hex: "22be87")
    static  let secondaryColor : Color = .white
    static  let grayColor : Color = CommonFunctionUtils.hexStringToUIColor(hex: "d7d7d7")
    static  let whiteColor : Color = .white
    static  let blackColor : Color = .black
    static  let loginBackGroundColor : Color = CommonFunctionUtils.hexStringToUIColor(hex: "F2F2F7")
    
    //MARK - String Constant
    
    //Login Screen
    static  let appTitle : String = "Health Care"
    static  let welcome : String = "Welcome"
    static  let signin : String =  "Sign in to your account"
    static  let email: String = "Email"
    static  let password : String = "Password"
    static  let forgotPassword : String = "Forgot Password ?"
    static  let logIn : String = "LogIn"
    static  let donthaveanaccount : String = "Don't have an account?"
    static  let registerHere : String = "Register Here"
    static  let emptyEmail : String = "Please enter email"
    static  let emptyPassword : String = "Please enter password"
    static  let validEmail : String = "Please enter valid email"
    static  let validPassword : String = "Password should contains uppercase , lowercase letter and special symbol"
    
    //Registration Screen
    static  let forgot_password : String = "Forgot Password"
    static  let signUp : String = "Sign Up"
    static  let info : String = "Info"
    static  let firstName : String = "First Name"
    static  let lastName : String = "Last Name"
    static  let confirmPassword : String = "Confirm Password"
    static  let phoneNumber : String = "Phone Number"
    static  let signUpAccount : String =  "Sign up for your account"
    static  let registeredAs : String =  "Registered As"
    static  let doctor : String =  "Doctor"
    static  let patient : String =  "Patient"
    static  let specialist : String =  "Specialist"
    static  let firstNameValidation : String =  "Please enter first name"
    static  let lastNameValidation : String =  "Please enter last name"
    static  let registeredAsValidation : String =  "Please select Registered As"
    static  let emptyPhone : String =  "Please enter phone number"
    static  let validPhoneNumber : String =  "Please enter valid phone number"
    static  let emptyConfirmPassword : String = "Please enter confirm password"
    static  let validConfirmPassword : String = "Confirm Password should be same as password"
    static  let forgot_password_text : String = "Please enter your email address to recieve a verification link."
    static  let forgot_password_msg : String = "Verification link sent to your email address."
    static  let ageEmpty : String = "Please enter age"
    static  let feeEmpty : String = "Please enter fee"
    static  let aboutEmpty : String = "Please enter about"
    static  let addressEmpty : String = "Please enter address"
    
    //Detail View
    static  let detail : String = "Detail"
    static  let fees : String = "Consultation Fees"
    
    //Doctor DashBoard
    static  let welcomeDoctor : String = "Welcome Doctor"
    static  let howWeCanHelp : String = "How we can help you Today?"
    static  let myPatient : String = "My Patient"
    static  let appointement : String = "Appointement"
    static  let profile : String = "Profile"
    static  let patientRequest : String = "Patient Request"
    static  let myCalendar : String = "My Calendar"
    static  let logoutConfirmMessgae : String = "Are you want to logout from the app?"
    
    //Patient DashBoard
    static  let welcomePatient : String = "Welcome"
    static  let search : String = "Search"
    static  let medicalFolder : String = "Medical Folder"
    static  let myDoctors : String = "My Doctors"
    static  let home : String = "Home"
    static  let myPatients : String = "My Patients"
    static  let patientsRequest : String = "Requests"
    static  let appointment : String = "Appointment"
    static  let chooseOption : String = "Choose an option"
    static  let search_doctor : String = "Search Doctor"
    static  let schedule_appointment : String = "Book Appointment"
    
    
    //Profile Page
    static  let editProfile : String = "Edit Profile"
    static  let male : String = "Male"
    static  let female : String = "Female"
    static  let gender : String = "Gender"
    static let is_walk_through_done : String = "is_walk_through_done"
    static let gotoWalkthrough : String = "gotoWalkthrough"
    static let patientDetails : String = "Patient Details"
    static let addPatient : String = "Add Patient"
    static let myProfile : String = "My Profile"
    static let save : String = "Save"
    static let consultationFee : String = "Consultation Fee"
    static let experiance : String = "Experiance"
    static let address : String = "Address"
    static let about : String = "About"
    static let age : String = "Age"
    static let submit : String = "Submit"
    static let phoneAlertMessage : String = "\n\n Simulator does not support this facility"
    static let decline : String = "Decline"
    static let accept : String = "Accept"
    static let none : String = "None"
    
    //Booking Appointment
    static let request_Accepted : String = "Request Accepted"
    static let request_Declined : String = "Request Declined"
    static let checkAvailability : String = "Check Availability"
    static let bookingAppointment : String = "Booking Appointment"
    static let no : String = "No"
    static let yesBookAppointment : String = "Yes, Book Appointment"
    static let confirmBookAppointmentMessage : String = "Appointment Booked Successfully"
    
    //Firebase chat
    static let timestamp : String = "timestamp"
    static let fromId : String = "fromId"
    static let toId : String = "toId"
    static let text_message = "text"
    static let uploaded_img = "uploaded_img"
    static let enter_message = "Enter your message here"
    static let lets_start_conversation = "Lets Start The Conversation!!"
    
    //Medical Records
    static let medical_records: String = "Medical Records"
    
    //MARK - Frame Size
    static  let fSize10 : CGFloat = 10
    static  let fSize20 : CGFloat = 20
    static  let fSize30 : CGFloat = 30
    static  let fSize40 : CGFloat = 40
    static  let fSize50 : CGFloat = 50
    static  let fSize60 : CGFloat = 60
    static  let fSize70 : CGFloat = 70
    static  let fSize80 : CGFloat = 80
    static  let fSize90 : CGFloat = 90
    static  let fSize120 : CGFloat = 120
    static  let fSize130 : CGFloat = 130
    static  let fSize140 : CGFloat = 140
    
    //MARK - Font Constant
    static  let font14 : CGFloat = 14
    static  let font18 : CGFloat = 18
    static  let font20 : CGFloat = 20
    static  let font24 : CGFloat = 24
    static  let font30 : CGFloat = 30
    static  let font40 : CGFloat = 40
    
    
    //MARK - Padding Constant
    static  let padding3 : CGFloat = 3
    static  let padding5 : CGFloat = 5
    static  let padding10 : CGFloat = 10
    static  let padding20 : CGFloat = 20
    static  let padding24 : CGFloat = 24
    static  let padding40 : CGFloat = 40
    
    struct FStore {
        static let doctorPatientRelation = "doctorPatientRelation"
        static let userFBCollection = "userInfo"
        static let appointmentFBCollection = "appointment"
        static let dateField = "date"
        static let chat_messages = "messages"
    }
    
    struct UserDefaultKey {
        static let isLogInSuccessful = "HealthLogIn"
        static let registeredAs = "HealthRegisteredAs"
        static let loggedInUserId = "loggedInUserId"
        static let userFirstName = "UserFirstName"
    }
}

