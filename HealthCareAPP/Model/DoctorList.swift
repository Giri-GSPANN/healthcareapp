//
//  GetUserInfo.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 06/12/22.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class DoctorList: ObservableObject{
    private let db = Firestore.firestore()
    @Published var list = [UserInfo]()
    
    init(){
        db.collection(Constants.FStore.userFBCollection).whereField("registeredAs", isEqualTo: Constants.doctor)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let userInfo =   try document.data(as: UserInfo.self)
                            self.list.append(userInfo)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                }
            }
    }
}
