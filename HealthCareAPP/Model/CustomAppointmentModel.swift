//
//  DoctorAppointmentModel.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 28/12/22.
//

import Foundation

struct CustomAppointmentModel : Codable, Hashable
{
    
    var userName : String
    var phoneNumber : String
    var email: String
    var appointment : Appointment
    var image : String
    
}
