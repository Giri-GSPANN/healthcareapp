//
//  PatientRequestListModel.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 23/12/22.
//

import Foundation

struct PatientRequestListModel : Hashable{
    let appointment : Appointment
    let firstName : String
    let lastName : String
    let phoneNumber : String
    let photo  : String

    enum CodingKeys: String, CodingKey {
        case appointment
        case firstName
        case lastName
        case phoneNumber
        case photo
    }
}
