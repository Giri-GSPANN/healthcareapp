//
//  UserInfo.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 30/11/22.
//

import Foundation

struct UserInfo : Codable, Hashable
{
    let userID : String
    let registeredAs : String
    let firstName : String
    let lastName : String
    let email : String
    let phoneNumber : String
    let photo  : String
    let gender  : String
    let speclistist  : String
    let about  : String
    let fee  : String
    let address  : String
    let experience  : String
    let age  : String
    let date = Date().timeIntervalSince1970
    
    var dictionary: [String: Any] {
        let data = (try? JSONEncoder().encode(self)) ?? Data()
        return (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) ?? [:]
    }
    
    enum CodingKeys: String, CodingKey {
        case userID
        case registeredAs
        case firstName
        case lastName
        case email
        case phoneNumber
        case photo
        case gender
        case speclistist
        case about
        case fee
        case address
        case experience
        case age
        case date
    }
}

