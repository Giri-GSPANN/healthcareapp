//
//  DoctorTimingsViewModel.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 20/12/22.
//

import SwiftUI

class DoctorTimingsViewModel: ObservableObject{
    
    @Published var doctorTimingsArray = [DoctorTimings]()
    
    init(date : String = CommonFunctionUtils.getTodayDate(format: "MM/dd/YY")){
        appendData(date: date)
    }
    
    // logic for random generator
    func getRandomStatus() -> String {
        let randomInt = Int.random(in: 0..<6)
        switch (randomInt){
        case 0:
            return AppointmentStatus.Booked.rawValue
        default:
            return AppointmentStatus.Available.rawValue
        }
    }
    
    // clear all data
    func clearData(){
        self.doctorTimingsArray.removeAll()
    }
    
    func appendData(date : String){
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "9:30 - 10:00 ",  aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "10:00 - 10:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "10:30 - 11:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "11:00 - 11:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "11:30 - 12:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "12:00 - 12:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "12:30 - 13:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "13:00 - 13:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "13:30 - 14:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "14:00 - 14:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "14:30 - 15:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "15:00 - 15:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "15:30 - 16:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "16:00 - 16:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "16:30 - 17:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "17:00 - 17:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "17:30 - 18:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "18:00 - 18:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "18:30 - 19:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "19:00 - 19:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "19:30 - 20:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "20:00 - 20:30 ", aptmt_status: AppointmentStatus.Available.rawValue))
        self.doctorTimingsArray.append(DoctorTimings(aptmt_date: date, aptmt_time: "20:30 - 21:00 ", aptmt_status: AppointmentStatus.Available.rawValue))
    }
    
    
}

enum AppointmentStatus : String{
    case Booked = "booked"
    case Available = "available"
    case Rejected = "rejected"
    case Accepted = "accepted"
    case Pending = "pending"
}
