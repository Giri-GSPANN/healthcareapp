//
//  AppointmentList.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 21/12/22.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class AppointmentList: ObservableObject{
    private let db = Firestore.firestore()
    @Published var list = [Appointment]()
    
    init(doctorID : String, date : String){
        if !doctorID.isEmpty && !date.isEmpty {
            fetchData(doctorID: doctorID, date: date)
        }
    }
    
    func fetchData(doctorID : String, date : String)
    {
        db.collection(Constants.FStore.appointmentFBCollection)
            .whereField("doctorID", isEqualTo: doctorID)
            .whereField("date", isEqualTo: date)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let appointmentInfo =   try document.data(as: Appointment.self)
                            self.list.append(appointmentInfo)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                }
            }
    }
}
