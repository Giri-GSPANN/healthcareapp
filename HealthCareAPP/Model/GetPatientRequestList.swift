//
//  GetPatientRequestList.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 23/12/22.
//


import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class GetPatientRequestList: ObservableObject{
    private let db = Firestore.firestore()
    private var list = [Appointment]()
    @Published var patientRequestlist = [PatientRequestListModel]()
    let userLoggedInID = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
    
    init(){
        getAppointmentList()
    }
    
    fileprivate func getAppointmentList() {
        db.collection(Constants.FStore.appointmentFBCollection)
            .whereField("doctorID", isEqualTo: userLoggedInID!)
            .whereField("actualStatus", isEqualTo: AppointmentStatus.Pending.rawValue)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let appointment =   try document.data(as: Appointment.self)
                            self.list.append(appointment)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                    //call
                    self.getPatientRequestObject()
                }
            }
    }
    
    func getPatientRequestObject(){
        for appointment in list {
            db.collection(Constants.FStore.userFBCollection)
                .whereField("userID", isEqualTo: appointment.patientID)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            do{
                                let userInfo =   try document.data(as: UserInfo.self)
                                let patientRequestListModel = PatientRequestListModel(appointment: appointment, firstName: userInfo.firstName, lastName: userInfo.lastName, phoneNumber: userInfo.phoneNumber, photo:userInfo.photo)
                                self.patientRequestlist.append(patientRequestListModel)
                            }
                            catch{
                                fatalError("Unable to decode document into user object")
                            }
                        }
                    }
                }
        }
        
    }
}
