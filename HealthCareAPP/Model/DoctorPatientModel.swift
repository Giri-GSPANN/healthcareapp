//
//  DoctorPatientModel.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 26/12/22.
//

import Foundation


struct DoctorPatientModel : Codable, Hashable
{
    let patientID : String
    let doctorID : String
    
    var dictionary: [String: Any] {
        let data = (try? JSONEncoder().encode(self)) ?? Data()
        return (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) ?? [:]
    }
    
    enum CodingKeys: String, CodingKey {
        case patientID
        case doctorID
    }
}
