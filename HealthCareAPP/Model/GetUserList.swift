//
//  GetPatientList.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 08/12/22.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class GetUserList: ObservableObject{
    private let db = Firestore.firestore()
    private var list = [DoctorPatientModel]()
    @Published var userInfoList = [UserInfo]()
    let userLoggedInID = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
    
    func getAppointmentList(userType: String) {
        userInfoList = [UserInfo]()
        db.collection(Constants.FStore.doctorPatientRelation)
            .whereField(userType, isEqualTo: userLoggedInID!)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let doctorPatientModel =   try document.data(as: DoctorPatientModel.self)
                            self.list.append(doctorPatientModel)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                    //call
                    self.getUserList(userType: userType)
                }
            }
    }
    
    func getUserList(userType: String){
        var userID : String
        var uniqueList : [DoctorPatientModel]
        
        if userType == "doctorID" {
            uniqueList = list.unique{$0.patientID}
        }else{
            uniqueList = list.unique{$0.doctorID}
        }
        
        for appointment in uniqueList {
            if userType == "doctorID"{
                userID = appointment.patientID}
            else{
                userID = appointment.doctorID
            }
            db.collection(Constants.FStore.userFBCollection)
                .whereField("userID", isEqualTo: userID)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            do{
                                let userInfo =   try document.data(as: UserInfo.self)
                                self.userInfoList.append(userInfo)
                            }
                            catch{
                                fatalError("Unable to decode document into user object")
                            }
                        }
                    }
                }
        }
        
    }
}


