//
//  GetPatientAppointmentList.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 28/12/22.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class GetPatientAppointmentList: ObservableObject{
    private let db = Firestore.firestore()
    var list = [Appointment]()
    @Published var doctorAppointmentModelList = [CustomAppointmentModel]()
    let userLoggedInID = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
    
    init(){
        fetchData()
    }
    
    func fetchData()
    {
        list = [Appointment]()
        doctorAppointmentModelList = [CustomAppointmentModel]()
        
        db.collection(Constants.FStore.appointmentFBCollection)
            .whereField("patientID", isEqualTo: userLoggedInID!)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let appointmentInfo =   try document.data(as: Appointment.self)
                            self.list.append(appointmentInfo)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                    
                    self.getDoctorDetails()
                }
            }
    }
    
    func getDoctorDetails(){
        for appointment in list {
            db.collection(Constants.FStore.userFBCollection)
                .whereField("userID", isEqualTo: appointment.doctorID)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            do{
                                let userInfo =   try document.data(as: UserInfo.self)
                                let userName = userInfo.firstName+" "+userInfo.lastName
                                
                                let doctorAppointmentModel = CustomAppointmentModel(userName: userName , phoneNumber: userInfo.phoneNumber, email: userInfo.email, appointment: appointment, image: userInfo.photo)
                                self.doctorAppointmentModelList.append(doctorAppointmentModel)
                            }
                            catch{
                                fatalError("Unable to decode document into user object")
                            }
                        }
                    }
                }
        }
    }
}
