//
//  DoctorTimings.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 15/12/22.
//
// MARK: - APPOINTMENT BOOKING AVAILABILITY MODEL

import SwiftUI

struct DoctorTimings: Identifiable {
    var id = UUID()
    var aptmt_date: String
    var aptmt_time: String
    var aptmt_status: String
}

