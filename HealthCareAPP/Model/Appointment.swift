//
//  Appointment.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 20/12/22.
//

import Foundation


struct Appointment : Codable, Hashable
{
    let appointmentID : String
    let patientID : String
    let doctorID : String
    let date : String
    let time : String
    let status : String
    let actualStatus : String
    let bookingTimeStamp = Date().timeIntervalSince1970
    
    var dictionary: [String: Any] {
        let data = (try? JSONEncoder().encode(self)) ?? Data()
        return (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) ?? [:]
    }
    
    enum CodingKeys: String, CodingKey {
        case appointmentID
        case patientID
        case doctorID
        case date
        case time
        case status
        case actualStatus
        case bookingTimeStamp
    }
}

