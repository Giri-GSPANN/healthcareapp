//
//  GetCalenderAppointmentsList.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 29/12/22.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class GetCalenderAppointmentsList: ObservableObject{
    private let db = Firestore.firestore()
    var list = [Appointment]()
    @Published var patientAppointmentModelList = [CustomAppointmentModel]()
    
    init(doctorID : String, date : String){
        if !doctorID.isEmpty && !date.isEmpty {
            fetchData(doctorID: doctorID, date: date)
        }
    }
    
    func fetchData(doctorID : String, date : String){
        db.collection(Constants.FStore.appointmentFBCollection)
            .whereField("doctorID", isEqualTo: doctorID)
            .whereField("date", isEqualTo: date)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        do{
                            let appointmentInfo =   try document.data(as: Appointment.self)
                            self.list.append(appointmentInfo)
                        }
                        catch{
                            fatalError("Unable to decode document into user object")
                        }
                    }
                    self.getPatientDetails()
                }
            }
    }
    
    func getPatientDetails(){
        for appointment in list {
            db.collection(Constants.FStore.userFBCollection)
                .whereField("userID", isEqualTo: appointment.patientID)
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            do{
                                let userInfo =   try document.data(as: UserInfo.self)
                                let userName = userInfo.firstName+" "+userInfo.lastName
                                
                                let doctorAppointmentModel = CustomAppointmentModel(userName: userName , phoneNumber: userInfo.phoneNumber, email: userInfo.email, appointment: appointment , image: userInfo.photo)
                                self.patientAppointmentModelList.append(doctorAppointmentModel)
                            }
                            catch{
                                fatalError("Unable to decode document into user object")
                            }
                        }
                    }
                }
        }
    }
}
