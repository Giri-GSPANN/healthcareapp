//
//  SplashScreen.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 05/12/22.
//

import SwiftUI
import Lottie

struct SplashView: View {
    
    @AppStorage(Constants.is_walk_through_done) var  walkthrough_done: Bool = false
    @AppStorage(Constants.gotoWalkthrough) var  gotoWalkthrough = false
    
    var body: some View {
        NavigationView{
            ZStack{
                if(gotoWalkthrough){
                    NavigationLink(destination: getDestination(),isActive: $gotoWalkthrough) {
                        EmptyView()
                    }
                }else{
                    PlaySplashView()
                }
            }.background(Constants.primaryColor)
                .onAppear {
                    gotoWalkthrough = false
                    DispatchQueue.main.asyncAfter(deadline: .now()+5) {
                        gotoWalkthrough = true
                    }
                }
        }.navigationViewStyle(.stack)
    }
    
    func getDestination() -> AnyView{
        if walkthrough_done {
            if  !UserDefaults.standard.bool(forKey: Constants.UserDefaultKey.isLogInSuccessful) {
                return AnyView(LogInView())
            }else{
                if  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.registeredAs) == Constants.doctor {
                    return AnyView(DoctorDashboardView())
                }
                else{
                    return AnyView(PatientDashboardView())
                }
            }
        }else{
            return AnyView(withAnimation(.easeIn){
                WalkThroughScreens()
            })
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}

struct PlaySplashView : View{
    @State var show = false
    var body: some View {
        VStack{
            Spacer(minLength: 100)
            HStack(alignment:.center){
                CaptionText().padding()
            }
            AnimatedView(show: $show)
            Spacer(minLength: 100)
        }
    }
}

struct AnimatedView: UIViewRepresentable {
    @Binding var show: Bool
    func makeUIView(context: Context) -> LottieAnimationView {
        let view = LottieAnimationView(name: "splash",bundle: Bundle.main)
        view.play()
        return view
    }
    func updateUIView(_ uiView: LottieAnimationView, context: Context) {
    }
}

struct CaptionText: View{
    var body: some View{
        Text("Health Care"+"\n"+" Done Smart.").foregroundColor(Color.white).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).font(.title).multilineTextAlignment(.center)
    }
}



