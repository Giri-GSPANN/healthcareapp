//
//  WalkThroughScreens.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 23/11/22.
//

import SwiftUI


struct WalkThroughScreens: View {
    
    var body: some View {            ZStack{
        // Changing between view : slider view
        TabView{
            ScreenView(image: "walkthrough_1", title: "Find Doctor Near You",supportIcon:"mappin.and.ellipse", details: "Find a doctor and make an appointment with them as you wish", bgColor: Color("wt_color_1"),showStart: false).transition(.scale)
            
            ScreenView(image: "walkthrough_2", title: "Fast Deliver",supportIcon: "bolt.car", details: "Get Medicin at you door step within 30 mints", bgColor: Color("wt_color_2"),showStart: false).transition(.scale)
            
            ScreenView(image: "walkthrough_3", title: "Save and Secure",
                       supportIcon: "hand.thumbsup.circle",details: "All of your Medical data save and secure with us", bgColor: Color("wt_color_2"),showStart:true).transition(.scale)
        }
        .padding(.bottom,30)
        .tabViewStyle(.page(indexDisplayMode: .always))
        .indexViewStyle(.page(backgroundDisplayMode: .always))
    }.background(Color(.white).ignoresSafeArea())
            .navigationBarBackButtonHidden(true)
    }
    
    struct WalkThroughScreens_Previews: PreviewProvider {
        static var previews: some View {
            WalkThroughScreens()
        }
    }
    
    struct ScreenView: View{
        @AppStorage(Constants.is_walk_through_done) var  walkthrough_done: Bool = false
        @AppStorage(Constants.gotoWalkthrough) var  gotoWalkthrough = false
        var image: String
        var title: String
        var supportIcon: String
        var details: String
        var bgColor: Color
        var showStart: Bool
        var body: some View{
            
            VStack{
                HStack(spacing: 10){
                    Text("Hello Member !").font(.title).fontWeight(.semibold)
                    //Letter Spacing
                        .kerning(1.4).padding()
                    Spacer()
                    Button (
                        action: {
                            withAnimation(.easeInOut){
                                gotoWalkthrough = true
                                walkthrough_done = true
                            }
                        },
                        label: {
                            if(showStart){
                                Text("Get Start").fontWeight(.semibold).kerning(1.2).foregroundColor(Constants.primaryColor)
                            }else{
                                Text("Skip").fontWeight(.semibold).kerning(1.2).foregroundColor(Constants.primaryColor)
                            }
                        }
                    ).padding()
                }
                Image(image).resizable().aspectRatio(contentMode: .fit)
            }.background(Color.white.ignoresSafeArea()).overlay(
                ZStack(alignment: .bottom){
                    VStack(spacing: 20){
                        HStack(spacing:20) {
                            Image(systemName: supportIcon).resizable().frame(width: 35, height: 35, alignment: .center)
                            Text(title).font(.title).fontWeight(.semibold).foregroundColor(.black)
                        }
                        Text(details).fontWeight(.none).kerning(1.3).multilineTextAlignment(.center).padding().foregroundColor(.secondary)
                        
                    }.padding(.top,10)
                }.padding(.top,380)
            )
        }
    }
    var totalPages = 3
}
