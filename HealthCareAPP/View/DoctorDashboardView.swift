//
//  DoctorDashboardView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 23/11/22.
//
import SwiftUI
import FirebaseAuth

struct DoctorDashboardView: View {
    @State private var isLogoutConfirmAlert : Bool = false
    
    var body: some View {
        
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text(Constants.home)
                            .fontWeight(.heavy)
                            .font(Font.system(size: Constants.font40))
                            .foregroundColor(.white)
                            .bold()
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: {
                            isLogoutConfirmAlert = true
                        }, label: {
                            Image(systemName: "power").foregroundColor(.white)
                        })
                    }
                } //toolbar
            
            VStack {
                VStack {
                    Text(Constants.welcomeDoctor)
                        .fontWeight(.bold).font(Font.system(size: Constants.font30))
                        .foregroundColor(.white)
                        .bold()
                        .padding(.top)
                    
                    Text(Constants.howWeCanHelp)
                        .fontWeight(.regular).font(Font.system(size: Constants.font20))
                        .foregroundColor(.white)
                    
                }.padding(.all)
                //Hello text
                
                Spacer()
                
                // 1st row
                HStack(spacing:Constants.fSize20){
                    NavigationLink(
                        destination: DoctorDetailsView(showEditOption:true),
                        label: {
                            DashboardButton(buttonName: Constants.profile,
                                            imageName: "list.bullet.rectangle",
                                            imgWidth: Constants.fSize40,
                                            imgHeight: Constants.fSize50,
                                            imgPadding:.top)
                        }) // profile
                    
                    NavigationLink(
                        destination: PatientRequestView(),
                        label: {
                            DashboardButton(buttonName: Constants.patientsRequest,
                                            imageName: "person.fill.badge.plus",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // Patient Request
                    
                }.padding(.horizontal)
                
                // 2nd row
                
                HStack(spacing:Constants.fSize20){
                    
                    NavigationLink(
                        destination: MyPatientView(),
                        label: {
                            DashboardButton(buttonName: Constants.myPatients,
                                            imageName: "person.fill",
                                            imgWidth: Constants.fSize40,
                                            imgHeight: Constants.fSize50,
                                            imgPadding:.top)
                        }) // My Pateint Button
                    
                    NavigationLink(
                        destination: BookAppointmentView(),
                        label: {
                            DashboardButton(buttonName: Constants.schedule_appointment,
                                            imageName: "calendar.badge.clock",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // Appointment Request
                    
                }.padding(.all)
                
                // 3rd row
                
                HStack(spacing:Constants.fSize40){
                    
                    NavigationLink(
                        destination: DoctorAppointmentView(),
                        label: {
                            DashboardButton(buttonName: Constants.myCalendar,
                                            imageName: "calendar",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // My Calendar
                    
                }.padding(.bottom)
            }
        }
        .background(Image("halfCircleBg")
            .aspectRatio(contentMode: ContentMode.fill))
        .navigationBarBackButtonHidden(true)
        .accentColor(.white)
        .navigationAppearance(backgroundColor:  UIColor(Constants.primaryColor), foregroundColor: .systemBackground, tintColor: .white, hideSeparator: true)
        .alert(isPresented: $isLogoutConfirmAlert) {
            Alert(
                title: Text(Constants.appTitle),
                message: Text(Constants.logoutConfirmMessgae),
                primaryButton: .default(
                    Text("Logout"),
                    action: {
                        logout()
                    }
                ),
                secondaryButton: .destructive(
                    Text("Cancel"),
                    action:{
                        isLogoutConfirmAlert = false
                    }
                )
            )
        }
    }
    
    private func logout(){
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.set(false, forKey: Constants.UserDefaultKey.isLogInSuccessful)
            UserDefaults.standard.set("", forKey: Constants.UserDefaultKey.registeredAs)
            NavigationUtil.popToRootView()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}

struct DoctorDashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorDashboardView()
    }
}

