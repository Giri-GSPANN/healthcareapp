//
//  RegistrationView.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 24/11/22.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct RegistrationView:  View {
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var email: String = ""
    @State private var phoneNumber: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var isHighlight = false
    @State private var showAlert: Bool = false
    @State private var message: String = ""
    @State private var registeredAs: String = Constants.doctor
    @State private var registeredAsTitle: String = ""
    @State private var selectedGender = Constants.male
    
    @State private var selectedSpecialistLabel = ""
    @State private var selectedSpecialist = Constants.specialistArray[0]
    
    @State private var isRegisterSuccessful: Bool = false
    private let db = Firestore.firestore()
    @State private var username = ""
    @State private var isOpenDoctorDashboard: Bool = false
    
    @State private var gender = ""
    @State private var isActivityIndicatorVisible: Bool = false
    
    @State private var avatarImage = UIImage(named: "person")!
    @State private var isShowingPhotoPicker = false
    @State private var imageSelected = false
    var body: some View {
        
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.signUp)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                } //toolbar
            
            VStack{
                Spacer()
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        
                        HStack() {
                            Spacer()
                            Image(uiImage: avatarImage).resizable().clipShape(Circle()).frame(width: 120, height: 120).onTapGesture {
                                isShowingPhotoPicker = true
                            }
                            Spacer()
                        }
                        
                        HStack(spacing: 20) {
                            
                            HealthCareTextField(placeHolder: Constants.registeredAs,field: $registeredAsTitle, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.primaryColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight, isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Registered As", selection: $registeredAs) {
                                ForEach(Constants.registeredAsArray, id: \.self) { value in
                                    Text(value)
                                }
                            }.pickerStyle(.segmented)
                        }
                        
                        HealthCareTextField(placeHolder: Constants.firstName,field: $firstName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.lastName,field: $lastName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress,autocapitalizationType: .none).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.phoneNumber,field: $phoneNumber, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "phone.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad).padding([ .vertical], Constants.padding5)
                        
                        HealthCareSecureField(placeHolder: Constants.password,field: $password,fontSize :Constants.font18, backgroundColor: Constants.whiteColor, textColor: Constants.blackColor, icon: "lock.fill", iconColor: Constants.grayColor,strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight)
                            .padding([.vertical], Constants.padding5)
                        
                        HealthCareSecureField(placeHolder: Constants.confirmPassword,field: $confirmPassword,fontSize :Constants.font18, backgroundColor: Constants.whiteColor, textColor: Constants.blackColor, icon: "lock.fill", iconColor: Constants.grayColor,strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight)
                            .padding([.vertical], Constants.padding5)
                        
                        HStack(spacing: 20) {
                            HealthCareTextField(placeHolder: Constants.gender,field: $gender, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Gender", selection: $selectedGender) {
                                ForEach(Constants.genderArray, id: \.self) {
                                    Text($0)
                                }
                            }.pickerStyle(.segmented)
                        }
                        if(registeredAs == Constants.doctor){
                            HStack(spacing : -40) {
                                HealthCareTextField(placeHolder: Constants.specialist,field: $selectedSpecialistLabel, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.primaryColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight, isDisable: true).padding([ .vertical], Constants.padding5)
                                
                                Picker("", selection: $selectedSpecialist) {
                                    ForEach(Constants.specialistArray, id: \.self) { value in
                                        Text(value)
                                    }
                                }.pickerStyle(.automatic)
                            }
                            
                        }
                    }
                }
                Spacer()
                
                ZStack{
                    HStack(){
                        NavigationLink(destination: getDestination(),isActive: $isRegisterSuccessful) {
                            Button(Constants.signUp) {
                                isValidateFiled()
                            }
                            .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                            .padding([.horizontal,.vertical], Constants.padding5)
                            .alert(isPresented: $showAlert) {
                                Alert(
                                    title: Text(Constants.appTitle),
                                    message: Text(message),
                                    dismissButton: .default(Text("Got it!"))
                                )
                            }
                        }.padding([.trailing], 32.0)
                            .padding([.leading], 32.0)
                    }.padding(.top)
                }
            }.progressDialog(isShowing: $isActivityIndicatorVisible, message: "Loading...")
                .sheet(isPresented: $isShowingPhotoPicker, content: {
                    PhotoPicker(avatarImage: $avatarImage, imageSelected: $imageSelected)
                })
        }.navigationAppearance(backgroundColor:  UIColor(Constants.primaryColor), foregroundColor: .systemBackground, tintColor: .white, hideSeparator: true)
    }
    
    private func registerUser(){
        isActivityIndicatorVisible = true
        print("Reigstraion detail : \(email) :: \(password)")
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            DispatchQueue.main.async {
                if let e = error{
                    isRegisterSuccessful = false
                    print("Reigstraion Error : \(e)")
                    isActivityIndicatorVisible = false
                    showAlert = true
                    message = e.localizedDescription
                }
                else{
                    guard let userID = authResult?.user.uid else{
                        fatalError("Unable to get the userId")
                    }
                    
                    guard let email = authResult?.user.email else{
                        fatalError("Unable to get the userId")
                    }
                    
                    if let index = email.firstIndex(of: "@") {
                        username = String(email.prefix(upTo: index))
                    }
                    
                    let imageData = avatarImage.jpegData(compressionQuality: 0.5)
                    
                    let imageBase64String = imageData?.base64EncodedString()
                    
                    let userInfo = UserInfo(userID: userID, registeredAs: registeredAs, firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber, photo: imageBase64String ?? "", gender: selectedGender, speclistist: selectedSpecialist, about: "", fee: "", address: "", experience: "", age: "")
                    
                    if(registeredAs == Constants.doctor){
                        isOpenDoctorDashboard = true
                    }
                    else{
                        isOpenDoctorDashboard = false
                    }
                    
                    db.collection(Constants.FStore.userFBCollection).document(userID).setData(userInfo.dictionary){
                        (error) in
                        if let e = error {
                            print("There was an issue saving data to firestore, \(e)")
                        } else {
                            print("Successfully saved data.")
                            DispatchQueue.main.async {
                                isRegisterSuccessful = true
                                
                                UserDefaults.standard.set(true, forKey: Constants.UserDefaultKey.isLogInSuccessful)
                                UserDefaults.standard.set(userInfo.registeredAs, forKey: Constants.UserDefaultKey.registeredAs)
                                UserDefaults.standard.set(userInfo.userID, forKey: Constants.UserDefaultKey.loggedInUserId)
                                UserDefaults.standard.set(userInfo.firstName, forKey: Constants.UserDefaultKey.userFirstName)
                            }
                        }
                        isActivityIndicatorVisible = false
                    }
                }
            }
        }
    }
    
    func getDestination() -> AnyView{
        if(isOpenDoctorDashboard){
            return AnyView( DoctorDashboardView())}else{
                return AnyView(PatientDashboardView())
            }
    }
    
    private func isValidateFiled(){
        if(registeredAs.isEmpty){
            message = Constants.registeredAsValidation
            showAlert = true
        }else{
            if(firstName.isEmpty){
                message = Constants.firstNameValidation
                showAlert = true
            }
            else{
                if(lastName.isEmpty){
                    message = Constants.lastNameValidation
                    showAlert = true
                }
                else{
                    if email.isEmpty  {
                        message = Constants.emptyEmail
                        showAlert = true
                    } else{
                        if Validation.isValidEmail(email: email){
                            if phoneNumber.isEmpty {
                                message = Constants.emptyPhone
                                showAlert = true
                            }
                            else if Validation.isValidPhone(phone: phoneNumber){
                                
                                if password.isEmpty {
                                    message = Constants.emptyPassword
                                    showAlert = true
                                }
                                else if Validation.isValidPassword(password: password){
                                    
                                    if confirmPassword.isEmpty {
                                        message = Constants.emptyConfirmPassword
                                        showAlert = true
                                    }
                                    else if password == confirmPassword{
                                        showAlert = false
                                        registerUser()
                                    }
                                    else{
                                        showAlert = true
                                        message = Constants.validConfirmPassword
                                    }
                                }
                                else{
                                    showAlert = true
                                    message = Constants.validPassword
                                }
                                
                            }
                            else{
                                showAlert = true
                                message = Constants.validPhoneNumber
                            }
                        }
                        else{
                            showAlert = true
                            message = Constants.validEmail
                        }
                    }
                }
            }
        }
    }
}

struct RegistrationUI_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
