//
//  ForgotPasswordView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 21/12/22.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct ForgotPasswordView:  View {
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var email: String = ""
    @State private var phoneNumber: String = ""
    @State private var isHighlight = false
    @State private var showAlert: Bool = false
    @State private var message: String = ""
    @State private var selectedGender = Constants.male
    @State private var isRegisterSuccessful: Bool = false
    @State private var username = ""
    @State private var gender = ""
    @State private var isActivityIndicatorVisible: Bool = false
    private let db = Firestore.firestore()
    @Environment(\.presentationMode) var presentation
    
    init() {
        //Use this if NavigationBarTitle is with Large Font
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        //Use this if NavigationBarTitle is with displayMode = .inline
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.black]
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.forgot_password)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }//toolbar
            VStack{
                
                Spacer()
                Image("forgot-password")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 400, height: 200)
                    .padding([.vertical], Constants.padding20)
                
                Spacer()
                Text(Constants.forgot_password_text)
                    .font(.system(size: 20, weight: .regular, design: .default))
                    .foregroundColor(.primary).padding([.horizontal], 30)
                
                Spacer()
                HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress,autocapitalizationType: .none).padding([ .horizontal], Constants.padding20)
                
                Divider().padding([.horizontal], 40)
                
                Spacer()
                
                ZStack{
                    HStack(){
                        Button(Constants.submit) {
                            isValidateFiled()
                        }
                        .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                        .padding([.horizontal,.vertical], Constants.padding10)
                        .alert(isPresented: $showAlert) {
                            Alert(
                                title: Text(Constants.appTitle),
                                message: Text(message),
                                dismissButton: .default(Text("OK"))
                            )
                        }
                    }.padding(.all)
                }
            }.progressDialog(isShowing: $isActivityIndicatorVisible, message: "Loading...")
        }
    }
    
    private func registerUser()
    {
        isActivityIndicatorVisible = true
        
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            DispatchQueue.main.async {
                if let e = error{
                    isRegisterSuccessful = false
                    print("ForgotPasswordView Error : \(e)")
                    isActivityIndicatorVisible = false
                    showAlert = true
                    message = e.localizedDescription
                }
                else{
                    //dismiss screen
                    isRegisterSuccessful = true
                    isActivityIndicatorVisible = false
                    showAlert = true
                    message = Constants.forgot_password_msg
                    presentation.wrappedValue.dismiss()
                }
            }
        }
    }
    
    private func isValidateFiled()
    {
        if email.isEmpty  {
            message = Constants.emptyEmail
            showAlert = true
        } else{
            if Validation.isValidEmail(email: email){
                registerUser()
            }
            else{
                showAlert = true
                message = Constants.validEmail
            }
        }
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
