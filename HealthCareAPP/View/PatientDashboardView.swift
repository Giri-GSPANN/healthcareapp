//
//  DoctorDashboardView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 6/12/22.
//spa
import SwiftUI
import FirebaseAuth

struct PatientDashboardView: View {
    @State private var isLogoutConfirmAlert : Bool = false
    @State private var userFirstName : String = ""
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text(Constants.home)
                            .fontWeight(.heavy)
                            .font(Font.system(size: Constants.font40))
                            .foregroundColor(.white)
                            .bold()
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: {
                            isLogoutConfirmAlert = true
                        }, label: {
                            Image(systemName: "power").foregroundColor(.white)
                        })
                    }
                } //toolbar
            
            VStack {
                VStack {
                    Text(Constants.welcomePatient + " " + (userFirstName))
                        .fontWeight(.bold).font(Font.system(size: Constants.font30))
                        .foregroundColor(.white)
                        .bold()
                        .padding(.top).onAppear(perform: getFirstName)
                    
                    Text(Constants.chooseOption)
                        .fontWeight(.regular).font(Font.system(size: Constants.font20))
                        .foregroundColor(.white)
                    
                }.padding(.all)
                //Hello text
                
                Spacer()
                
                // 1st row
                HStack(spacing:Constants.fSize40){
                    
                    NavigationLink(
                        destination: PatientDetailsView(showEditOption: true),
                        label: {
                            DashboardButton(buttonName: Constants.profile,
                                            imageName: "list.bullet.rectangle",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // profile
                    
                    NavigationLink(
                        destination: SearchView(),
                        label: {
                            DashboardButton(buttonName: Constants.search_doctor,
                                            imageName: "magnifyingglass.circle",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // Search Button
                    
                }.padding(.horizontal)
                
                // 2nd row
                
                HStack(spacing:Constants.fSize40){
                    NavigationLink(
                        destination: MyDoctorsView(),
                        label: {
                            DashboardButton(buttonName: Constants.myDoctors,
                                            imageName: "stethoscope.circle",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // myDoctors
                    
                    NavigationLink(
                        destination: PatientAppointmentView(),
                        label: {
                            DashboardButton(buttonName: Constants.appointment,
                                            imageName: "calendar.badge.clock",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // appointment
                    
                }.padding(.all)
                
                // 3rd row
                HStack(spacing:Constants.fSize40){
                    
                    NavigationLink(
                        destination: MedicalRecordsView(),
                        label: {
                            DashboardButton(buttonName: Constants.medicalFolder,
                                            imageName: "folder",
                                            imgWidth: Constants.fSize50,
                                            imgHeight: Constants.fSize60,
                                            imgPadding:.top)
                        }) // Medical Folder
                    
                }.padding(.bottom)
            }
            
        }
        .background(Image("halfCircleBg")
            .aspectRatio(contentMode: ContentMode.fill))
        .navigationBarBackButtonHidden(true)
        .accentColor(.white).navigationAppearance(backgroundColor:  UIColor(Constants.primaryColor), foregroundColor: .systemBackground, tintColor: .white, hideSeparator: true)
        .accentColor(.white)
        .navigationAppearance(backgroundColor:  UIColor(Constants.primaryColor), foregroundColor: .systemBackground, tintColor: .white, hideSeparator: true)
        .alert(isPresented: $isLogoutConfirmAlert) {
            Alert(
                title: Text(Constants.appTitle),
                message: Text(Constants.logoutConfirmMessgae),
                primaryButton: .default(
                    Text("Logout"),
                    action: {
                        logout()
                    }
                ),
                secondaryButton: .destructive(
                    Text("Cancel"),
                    action:{
                        isLogoutConfirmAlert = false
                    }
                )
            )
        }
    }
    
    private func getFirstName()  {
        userFirstName =  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.userFirstName)!
    }
    
    private func logout(){
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.set(false, forKey: Constants.UserDefaultKey.isLogInSuccessful)
            UserDefaults.standard.set("", forKey: Constants.UserDefaultKey.registeredAs)
            NavigationUtil.popToRootView()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}

struct PatientDashboardView_Previews: PreviewProvider {
    static var previews: some View {
        PatientDashboardView()
    }
}


