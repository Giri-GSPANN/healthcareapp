//
//  AddPatientView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 09/12/22.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct AddPatientView:  View {
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var email: String = ""
    @State private var phoneNumber: String = ""
    @State private var isHighlight = false
    @State private var showAlert: Bool = false
    @State private var message: String = ""
    @State private var selectedGender = Constants.male
    @State private var isPatientAddedSuccessfully: Bool = false
    @State private var username = ""
    @State private var gender = ""
    @State private var isActivityIndicatorVisible: Bool = false
    private let db = Firestore.firestore()
    @Environment(\.presentationMode) var presentation
    
    init() {
        //Use this if NavigationBarTitle is with Large Font
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        //Use this if NavigationBarTitle is with displayMode = .inline
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.black]
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.addPatient)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
            VStack{
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        
                        HealthCareTextField(placeHolder: Constants.firstName,field: $firstName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.lastName,field: $lastName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress,autocapitalizationType: .none).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.phoneNumber,field: $phoneNumber, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "phone.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad).padding([ .vertical], Constants.padding5)
                        
                        
                        HStack(spacing: 20) {
                            HealthCareTextField(placeHolder: Constants.gender,field: $gender, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Gender", selection: $selectedGender) {
                                ForEach(Constants.genderArray, id: \.self) {
                                    Text($0)
                                }
                            }.pickerStyle(.segmented)
                        }
                    }
                }
                Spacer()
                ZStack{
                    HStack(){
                        Button(Constants.submit) {
                            isValidateFiled()
                        }
                        .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                        .padding([.horizontal,.vertical], Constants.padding10)
                        .alert(isPresented: $showAlert) {
                            Alert(
                                title: Text(Constants.appTitle),
                                message: Text(message),
                                dismissButton: .default(Text("OK"))
                            )
                        }
                    }.padding(.all)
                }
            }.progressDialog(isShowing: $isActivityIndicatorVisible, message: "Loading...")
        }
    }
    
    private func registerUser()
    {
        isActivityIndicatorVisible = true
        
        Auth.auth().createUser(withEmail: email, password: "Test@123") { authResult, error in
            DispatchQueue.main.async {
                if let e = error{
                    isPatientAddedSuccessfully = false
                    print("Reigstraion Error : \(e)")
                    isActivityIndicatorVisible = false
                    showAlert = true
                    message = e.localizedDescription
                }
                else{
                    guard let userID = authResult?.user.uid else{
                        fatalError("Unable to get the userId")
                    }
                    
                    guard let email = authResult?.user.email else{
                        fatalError("Unable to get the userId")
                    }
                    
                    if let index = email.firstIndex(of: "@") {
                        username = String(email.prefix(upTo: index))
                    }
                    
                    let userInfo = UserInfo(userID: userID, registeredAs: Constants.patient, firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber, photo: "", gender: selectedGender, speclistist: "", about: "", fee: "", address: "", experience: "", age: "")
                    
                    db.collection(Constants.FStore.userFBCollection).document(userID).setData(userInfo.dictionary){
                        (error) in
                        if let e = error {
                            print("There was an issue saving data to firestore, \(e)")
                        } else {
                            print("Successfully saved data.")
                            let doctorID = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
                            createRelation(patientId: userID, doctorId: doctorID!)
                        }
                        
                    }
                }
            }
        }
    }
    
    private func isValidateFiled()
    {
        if(firstName.isEmpty){
            message = Constants.firstNameValidation
            showAlert = true
        }
        else{
            if(lastName.isEmpty){
                message = Constants.lastNameValidation
                showAlert = true
            }
            else{
                if email.isEmpty  {
                    message = Constants.emptyEmail
                    showAlert = true
                } else{
                    if Validation.isValidEmail(email: email){
                        if phoneNumber.isEmpty {
                            message = Constants.emptyPhone
                            showAlert = true
                        }
                        else if !Validation.isValidPhone(phone: phoneNumber){
                            showAlert = true
                            message = Constants.validPhoneNumber
                        }else{
                            registerUser()
                        }
                    }
                    else{
                        showAlert = true
                        message = Constants.validEmail
                    }
                }
            }
        }
    }
    
    func createRelation(patientId:String, doctorId:String){
        let db = Firestore.firestore()
        db.collection(Constants.FStore.doctorPatientRelation).document().setData(["patientID": patientId, "doctorID" : doctorId]){
            (error) in
            if let e = error {
                print("There was an issue saving data to firestore, \(e)")
            } else {
                print("Successfully saved data.")
                DispatchQueue.main.async {
                    isPatientAddedSuccessfully = true
                    presentation.wrappedValue.dismiss()
                }
            }
            isActivityIndicatorVisible = false
        }
    }
}

struct AddPatientView_Previews: PreviewProvider {
    static var previews: some View {
        AddPatientView()
    }
}
