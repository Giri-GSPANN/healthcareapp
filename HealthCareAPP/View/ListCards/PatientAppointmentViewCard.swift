//
//  PatientAppointmentViewCard.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 22/12/22.


import SwiftUI

struct PatientAppointmentViewCard: View {
    var time : String
    var appointmentID : String
    var patientName : String
    var phoneNumber : String
    var email: String
    var patientId: String
    var image: String
    
    @State private var openChatView: Bool = false
    
    var body: some View {
        VStack{
            HStack{
                Rectangle().foregroundColor(Constants.primaryColor).frame(width: 110, height:130).overlay {
                    VStack{
                        Text(time).foregroundColor(.white).font(.system(size: 10)).padding(7).overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(.white)
                        )
                        Text("Appointment ID: \(appointmentID)").font(.system(size: 11)).foregroundColor(.white).bold().multilineTextAlignment(.center).padding(2)
                    }
                }
                
                VStack(alignment:.leading){
                    Text(patientName).lineLimit(1).foregroundColor(Constants.primaryColor).font(.system(size: 14).bold())
                    HStack{
                        Image(systemName: "envelope.fill").padding(-2)
                        //TODO: replace with Doctor EmailId
                        Text(email).lineLimit(1).font(.system(size: 12))
                        Spacer()
                        
                    }.foregroundColor(.gray)
                    HStack{
                        Image(systemName: "phone.fill").padding(-2)
                        //TODO: replace with Patient PH.NO
                        Text(phoneNumber).lineLimit(1).font(.system(size: 12))
                        Spacer()
                        
                    }.foregroundColor(.gray)
                    HStack{
                        HStack{
                            Image(systemName:"text.bubble")
                                .resizable()
                                .foregroundColor(.blue)
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 20, height: 20)
                            Text("Click to chat").lineLimit(1).font(.system(size: 12)).foregroundColor(.blue)
                        }.onTapGesture{
                            self.openChatView = true
                        }
                        
                        NavigationLink(
                            destination: OpenChatView(chatUserId: patientId,chatUserName: patientName, chatUserImage: image),
                            isActive: $openChatView){}.opacity(0)
                    }
                    
                }.padding([.all])
            }.cornerRadius(10)
        }
        .background(Color.white).cornerRadius(10).padding([.leading,.trailing],5)
    }
}

struct PatientAppointmentViewCard_Previews: PreviewProvider {
    static var previews: some View {
        PatientAppointmentViewCard(time: "12.00-12.30", appointmentID: "sfasafdscxfw77", patientName: "Rahul Dao", phoneNumber: "89878976876", email: "test@gmail.com", patientId: "", image: "")
    }
}
