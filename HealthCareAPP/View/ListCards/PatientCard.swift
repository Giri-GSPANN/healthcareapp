//
//  PatientCard.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 07/12/22.
//

import SwiftUI

struct PatientCard: View {
    var patientId: String
    var image: String
    var name: String
    var phoneNumber: String
    var text: String
    
    @State private var showingAlert = false
    @State private var avatarImage = UIImage(named: "person")!
    
    @State private var openChatView: Bool = false
    
    var body: some View {
        HStack(alignment: .center) {
            
            let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            
            Image(uiImage: decodedimage ?? avatarImage)
                .resizable().clipShape(Circle())
                .aspectRatio(contentMode: .fit)
                .frame(width: 50)
                .padding(.all, 5)
            
            VStack(alignment: .leading) {
                
                Text(text)
                    .font(.system(size: 16, weight: .regular, design: .default))
                    .foregroundColor(.gray)
                
                Text(name)
                    .font(.system(size: 18, weight: .bold, design: .default))
                    .foregroundColor(Constants.primaryColor)
                
            }.padding(.trailing, 10)
                .padding(.top, 15)
                .padding(.bottom, 15)
            
            Spacer()
            
            HStack(alignment: .lastTextBaseline){
                
                Image(systemName:"phone.fill")
                    .resizable()
                    .foregroundColor(Constants.primaryColor)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .padding(.all, 5)
                    .onTapGesture{
                        showingAlert = true
                    }
                    .alert( "Dialing Number is:\n" + phoneNumber + Constants.phoneAlertMessage, isPresented: $showingAlert) {
                        Button("OK", role: .cancel) {
                            showingAlert = false
                        }
                    }
                
                Image(systemName:"text.bubble")
                    .resizable()
                    .foregroundColor(.blue)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .padding(.all, 5).onTapGesture {
                        self.openChatView = true
                    }
                
                NavigationLink(
                    destination: OpenChatView(chatUserId: patientId,chatUserName: name, chatUserImage: image),
                    isActive: $openChatView){}.opacity(0)
                
            }.padding(.trailing, 10).frame(width: 100, height: 0)
            
            
        }
        .frame(maxWidth: .infinity)
        .background(.white)
        .modifier(CardModifier())
        .padding([.horizontal], 5)
    }
}

struct PatientCard_Previews: PreviewProvider {
    static var previews: some View {
        PatientCard(patientId: "", image: "", name: "David King", phoneNumber: "8087586981" , text: "Patient name:")
    }
}
