//
//  RequestCard.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 12/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct RequestCard: View {
    var image: String
    var name: String
    var phoneNumber: String
    var status: String
    var appointment : Appointment
    var updatePatientRequestList : (String) -> ()
    
    @State private var showingAlert = false
    @State private var avatarImage = UIImage(named: "person")!
    @State private var isLoading = false
    
    var body: some View {
        
        HStack(alignment: .center) {
            
            let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            
            Image(uiImage: decodedimage ?? avatarImage)
                .resizable().clipShape(Circle())
                .aspectRatio(contentMode: .fit)
                .frame(width: 80)
                .padding(.all, 5)
            
            VStack(alignment:.leading) {
                
                Text(name)
                    .font(.system(size: 20, weight: .bold, design: .default))
                    .foregroundColor(Constants.primaryColor)
                
                Text(phoneNumber)
                    .font(.system(size: 16, weight: .regular, design: .default))
                    .foregroundColor(.gray)
                    .padding(.bottom, 2)
                
                Text("Appointment Date: \(appointment.date)")
                    .font(.system(size: 16, weight: .regular, design: .default))
                    .foregroundColor(.black)
                
                Text("Appointment Time: \(appointment.time)")
                    .font(.system(size: 16, weight: .regular, design: .default))
                    .foregroundColor(.black)
                    .padding(.bottom, 2)
                
                //Actual status is pending
                HStack(alignment: .lastTextBaseline){
                    
                    Button(Constants.decline) {
                        //MARK: need to add user in relation table
                        updateAppointment(appointmentId: appointment.appointmentID, status:AppointmentStatus.Available.rawValue, actualStatus: AppointmentStatus.Rejected.rawValue)
                        isLoading = true
                        
                    }.buttonStyle(HealthCareButton(fontSize :Constants.font20,backgroundColor: Color.red,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor, height: 35))
                    
                    Button(Constants.accept) {
                        //MARK: need to add user in relation table
                        updateAppointment(appointmentId: appointment.appointmentID, status:AppointmentStatus.Booked.rawValue, actualStatus: AppointmentStatus.Accepted.rawValue)
                        isLoading = true
                        
                    }.buttonStyle(HealthCareButton(fontSize :Constants.font20,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor,height: 35))
                }.padding(.bottom, 5)
                
            }.padding(.all, 5)
            
            Spacer()
        }.progressDialog(isShowing: $isLoading, message: "Updating Status...")
            .frame(maxWidth: .infinity)
            .background(.white)
            .cornerRadius(10)
            .padding([.horizontal],2)
    }
    
    func updateAppointment(appointmentId : String, status: String, actualStatus: String ){
        let db = Firestore.firestore()
        let docRef = db.collection(Constants.FStore.appointmentFBCollection).document(appointmentId)
        let appointmentData =  Appointment(appointmentID: appointmentId,
                                           patientID: appointment.patientID,
                                           doctorID: appointment.doctorID,
                                           date: appointment.date,
                                           time: appointment.time,
                                           status: status,
                                           actualStatus: actualStatus)
        
        docRef.setData(appointmentData.dictionary){ error in
            if let error = error {
                print("Error writing document: \(error)")
            } else {
                print("Document successfully written!")
                isLoading = false
                self.updatePatientRequestList(appointmentId)
                if(actualStatus == AppointmentStatus.Accepted.rawValue){
                    createRelation(patientId: appointment.patientID,
                                   doctorId: appointment.doctorID)
                }
            }
        }
        
    }
    
    func createRelation(patientId:String, doctorId:String){
        let db = Firestore.firestore()
        db.collection(Constants.FStore.doctorPatientRelation).document().setData(["patientID": patientId, "doctorID" : doctorId]){
            (error) in
            if let e = error {
                print("There was an issue saving data to firestore, \(e)")
            } else {
                print("Successfully saved data.")
                
            }
        }
    }
    
    func findRelation(patientId:String, doctorId:String){
        let db = Firestore.firestore()
        db.collection(Constants.FStore.doctorPatientRelation)
            .whereField("patientID", isEqualTo: patientId)
            .whereField("doctorID", isEqualTo: doctorId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Relation added: \(err)")
                    createRelation(patientId: patientId, doctorId: doctorId)
                } else {
                    print("Already relation added")
                }
            }
    }
    
}
