//
//  ProductCard.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 03/12/22.
//

import SwiftUI

struct DoctorCard: View {
    var doctorId: String
    var image: String
    var title: String
    var type: String
    var address: String
    var price: Double
    
    var body: some View {
        HStack(alignment: .center) {
            
            if let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters) {
                if let decodedimage = UIImage(data: dataDecoded){
                    Image(uiImage: decodedimage)
                        .resizable().clipShape(Circle())
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 50)
                        .padding(.all, 5)
                }else{
                    Image("person")
                        .resizable().clipShape(Circle())
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 50)
                        .padding(.all, 5)
                }
            }
            else{
                Image("person")
                    .resizable().clipShape(Circle())
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 50)
                    .padding(.all, 5)
            }
            
            VStack(alignment: .leading) {
                Text(title)
                    .font(.system(size: 18, weight: .bold, design: .default))
                    .foregroundColor(.primary)
                Text(type)
                    .font(.system(size: 16, weight: .bold, design: .default))
                    .foregroundColor(.gray)
                
                Text(address)
                    .font(.system(size: 16, weight: .bold, design: .default))
                    .foregroundColor(.gray)
                
                HStack {
                    Text("Rs " + String.init(format: "%0.2f", price))
                        .font(.system(size: 16, weight: .bold, design: .default))
                        .foregroundColor(Constants.primaryColor)
                }
            }.padding(.trailing, 10)
                .padding(.top, 3)
                .padding(.bottom, 3)
            Spacer()
        }
        .frame(maxWidth: .infinity, alignment: .center)
        .background(.white)
        .modifier(CardModifier())
        .padding([.horizontal], 5)
    }
}

struct ProductCard_Previews: PreviewProvider {
    static var previews: some View {
        DoctorCard(doctorId: "", image: "person", title: "Autumn Soup", type: "Entee", address: "location", price: 11.99)
    }
}
