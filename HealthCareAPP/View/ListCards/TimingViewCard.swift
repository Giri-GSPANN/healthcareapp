//
//  TimingViewCard.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 15/12/22.
//

import SwiftUI

struct TimingViewCard: View {
    
    var aptmt_time: String
    var aptmt_status: String
    
    var body: some View {
        ZStack{
          
            VStack(alignment: .center, spacing: 0){
                Text(aptmt_time).multilineTextAlignment(.center).font(.headline).foregroundColor(.white)
                Text(aptmt_status).multilineTextAlignment(.center).font(.subheadline).foregroundColor(.white).padding([.top])
            }
        }.frame(height: 100, alignment: .center)
    }
}

struct TimingViewCard_Previews: PreviewProvider {
    static var previews: some View {
        TimingViewCard(aptmt_time: "", aptmt_status: "")
    }
}
