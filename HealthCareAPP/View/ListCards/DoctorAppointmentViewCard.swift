//
//  DoctorAppointmentViewCard.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 21/12/22.


import SwiftUI

struct DoctorAppointmentViewCard: View {
    var date : String
    var time : String
    var appointmentID : String
    var doctorName : String
    var phoneNumber : String
    var apptStatus: String
    var email: String
    var doctorId: String
    var image: String
    
    
    @State private var openChatView: Bool = false
    
    var body: some View {
        VStack{
            HStack{
                Rectangle().foregroundColor(Constants.primaryColor).frame(width: 110, height:130).overlay {
                    VStack{
                        
                        Text(date).foregroundColor(.white).fontWeight(.bold).font(.subheadline).padding(.top,5)
                        
                        
                        Text(time).foregroundColor(.white).font(.system(size: 10)).padding(7).overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(.white)
                        )
                        
                        Text("Appointment ID: \(appointmentID)").font(.system(size: 11)).foregroundColor(.white).bold().multilineTextAlignment(.center).padding(2)
                    }
                }
                VStack(alignment:.leading){
                    
                    Text("Dr. \(doctorName)").lineLimit(1).multilineTextAlignment(.center).font(.system(size: 14).bold()).foregroundColor(Constants.primaryColor)
                    HStack{
                        Image(systemName: "envelope.fill").padding(-2)
                        
                        Text(email).lineLimit(1).font(.system(size: 12)).multilineTextAlignment(.leading)
                        Spacer()
                        
                    }.foregroundColor(.gray)
                    HStack{
                        Image(systemName: "phone.fill").padding(-2)
                        //TODO: replace with Doctor Ph.No
                        Text(phoneNumber).lineLimit(1).font(.system(size: 12))
                        Spacer()
                        
                    }.foregroundColor(.gray)
                    
                    HStack{
                        Image(systemName:"text.bubble")
                            .resizable()
                            .foregroundColor(.blue)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 20, height: 20)
                        Text("Click to chat").lineLimit(1).font(.system(size: 12)).foregroundColor(.blue)
                    }.onTapGesture{
                        self.openChatView = true
                    }
                    
                    NavigationLink(
                        destination: OpenChatView(chatUserId: doctorId,chatUserName: doctorName, chatUserImage: image),
                        isActive: $openChatView){}.opacity(0)
                    
                }.padding([.all], 5)
                Spacer()
                VStack{
                    if apptStatus.elementsEqual(AppointmentStatus.Accepted.rawValue){
                        Text("Accepted").foregroundColor(.black).font(.system(size: 12)).bold().kerning(1).padding(7).overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Constants.primaryColor))
                    } else if apptStatus.elementsEqual(AppointmentStatus.Rejected.rawValue){
                        Text("Rejected").foregroundColor(.black).font(.system(size: 12)).bold().kerning(1).padding(7).overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(.red))
                    }else{
                        Text("Pending").foregroundColor(.black).font(.system(size: 12)).bold().kerning(1).padding(7).overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(.yellow))
                    }
                }.padding([.trailing],5)
            }.cornerRadius(10)
        }
        .background(Color.white).cornerRadius(10).padding([.leading,.trailing], Constants.padding10)
    }
}

struct DoctorAppointmentViewCard_Previews: PreviewProvider {
    static var previews: some View {
        DoctorAppointmentViewCard(date: "22/12/2022", time: "10.30-11.00", appointmentID: "1SgEle70Zi60QxBuIGf8", doctorName: "David King", phoneNumber: "9850986984", apptStatus: "accepted", email: "suhas@gmail.com", doctorId: "", image: "")
    }
}
