//
//  PatientRequestView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 12/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct PatientRequestView: View {
    
    @State private var searchText = ""
    @StateObject var patientRequestArray = GetPatientRequestList()
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("Test")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.patientsRequest)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
            
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                List {
                    ForEach(searchResults, id: \.self) { data in
                        let username = data.firstName + " " + data.lastName
                        RequestCard(image: data.photo, name: username, phoneNumber: data.phoneNumber, status: Constants.none, appointment:data.appointment, updatePatientRequestList: self.updatePatientRequestList)
                    }
                    .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                    .listRowSeparator(.hidden)
                    .listRowBackground(Constants.grayColor)
                }
                .listStyle(.plain)
                
            }
            VStack{
                if(searchResults.isEmpty){
                    ShowEmptyView(animationName: "apt", description: "Appointment Requests Not Available")
                }
            }
        }.accentColor(.white)
        
    }
    
    var searchResults: [PatientRequestListModel] {
        let sortedList = patientRequestArray.patientRequestlist.sorted{ $0.appointment.date.compare( $1.appointment.date) == .orderedAscending}
        
        if searchText.isEmpty {
            return sortedList
        } else {
            return sortedList.filter { $0.firstName.localizedCaseInsensitiveContains(searchText) }
        }
    }
    
    func updatePatientRequestList(id : String) {
        print("do something : \(id)")
        if let row = patientRequestArray.patientRequestlist.firstIndex(where: {$0.appointment.appointmentID  == id}) {
            patientRequestArray.patientRequestlist.remove(at: row)
        }
    }
}

struct PatientRequestView_Previews: PreviewProvider {
    static var previews: some View {
        PatientRequestView()
    }
}


