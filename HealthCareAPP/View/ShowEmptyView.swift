//
//  ShowEmptyView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 05/01/23.
//

import SwiftUI
import Lottie

struct ShowEmptyView: View {
    @State var animationName: String = ""
    @State var description: String = ""
    
    var body: some View {
        ZStack{
            VStack{
                LottieAnimatedView(animationName: animationName)
                HStack(alignment:.center){
                    Text(description).fontWeight(.medium).foregroundColor(.black).multilineTextAlignment(.center)
                }
                Spacer(minLength: 100)
            }
        }
    }
}

struct LottieAnimatedView: UIViewRepresentable {
    @State var animationName: String
    func makeUIView(context: Context) -> LottieAnimationView {
        let view = LottieAnimationView(name: animationName,bundle: Bundle.main)
        view.play()
        return view
    }
    
    func updateUIView(_ uiView: LottieAnimationView, context: Context) {
    }
}



