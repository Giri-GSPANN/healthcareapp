//
//  MessageBubble.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 28/12/22.
//
import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import SwiftUI
import FirebaseCoreInternal
import FirebaseFirestoreSwift

struct ChatMessage: Identifiable, Decodable, Hashable {
    var id: String{documentId}
    let documentId: String
    let fromId,uploaded_img, toId, textMessage: String

    init(doucmentId: String, data: [String: Any]){
        self.documentId = doucmentId
        self.fromId = data[Constants.fromId] as? String ?? ""
        self.textMessage = data[Constants.text_message] as? String ?? ""
        self.toId = data[Constants.toId] as? String ?? ""
        self.uploaded_img = data[Constants.uploaded_img] as? String ?? ""
    }
}
struct MedicalRecord: Identifiable, Decodable, Hashable {
    var id: String{documentId}
    var documentId: String
    let fromId,uploaded_img: String
    
    init(doucmentId: String, data: [String: Any]){
        self.documentId = doucmentId
        self.fromId = data[Constants.fromId] as? String ?? ""
        self.uploaded_img = data[Constants.uploaded_img] as? String ?? ""
    }
    
}

class MessagesManager: ObservableObject {
    @Published private(set) var chatMessages: [ChatMessage] = []
    @Published private(set) var medicalRecords : [MedicalRecord] = []
    @Published private(set) var lastMessageId: String = ""
    
    // Create an instance of our Firestore database
    let db = Firestore.firestore()
    // Get User Medical records using his userId
    func getMedicalRecords(userId: String) {
        db.collection("MedicalRecords")
            .document(userId)
            .collection(userId)
            .order(by: Constants.timestamp)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("FAILED TO FETCH MESSAGES\(error)")
                    return
                }
                querySnapshot?.documents.forEach({ queryDocument in
                   let data = queryDocument.data()
                    let docId = queryDocument.documentID
                    self.medicalRecords.append(.init(doucmentId: docId, data: data))
                })
            }
    }
    
    // Read message from Firestore in real-time with the addSnapShotListener
    func getMessages(fromId: String, toID: String) {
        db.collection(Constants.FStore.chat_messages)
            .document(fromId)
            .collection(toID)
            .order(by: Constants.timestamp)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("FAILED TO FETCH MESSAGES\(error)")
                    return
                }
                querySnapshot?.documentChanges.forEach({ change in
                    if change.type == .added{
                        let data = change.document.data()
                        self.chatMessages.append(.init(doucmentId: change.document.documentID, data: data))
                    }
                   
                })
                // Getting the ID of the last message so we automatically scroll to it in ContentView
                if let id = self.chatMessages.last?.id {
                    self.lastMessageId = id
                }
                
            }
          }
    
    // Add a message in Firestore
    func sendMessage(fromId: String, toID: String, textMessage: String, uploadImg: String) {
        // Create a new document in Firestore with the newMessage variable above, and use setData(from:) to convert the Message into Firestore data
        let document = db.collection(Constants.FStore.chat_messages)
            .document(fromId)
            .collection(toID)
            .document()
        
        // Create a new Message instance, with a unique ID, the text we passed, and a timestamp
        let messageData = [Constants.fromId: fromId, Constants.toId: toID, Constants.text_message: textMessage,Constants.uploaded_img: uploadImg, Constants.timestamp: Timestamp()] as [String : Any]
          document.setData(messageData){ error in
            if let error = error {
                print("current_Failed to save message into Firestore: \(error)")
                return
            }
            print("curreent_successfully saved message into Firestore")
        }
        
        //Store the Uploaded Document to the User Medical Record Folder --> Either patient send or doctor send always store in patient side.
        if(!uploadImg.isEmpty){
        let medicalRecordData = [Constants.fromId: fromId, Constants.uploaded_img: uploadImg, Constants.timestamp: Timestamp()] as [String : Any]
        let registeredAs = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.registeredAs)!
        let userMedicalRecordDocument = db.collection("MedicalRecords")
            .document(registeredAs.elementsEqual("Patient") ? fromId : toID)
        .collection(registeredAs.elementsEqual("Patient") ? fromId : toID)
        .document()
        userMedicalRecordDocument.setData(medicalRecordData) { error in
            if let error = error {
                print("medical_record_failed to save into Firestore: \(error)")
                return
            }
            print("medical_record_successfully saved into Firestore")
        }
        }
        
        
        // Recepient should also get instant message
        let recepientMessageDocument = db.collection(Constants.FStore.chat_messages)
            .document(toID)
            .collection(fromId)
            .document()
        recepientMessageDocument.setData(messageData){ error in
            if let error = error {
                print("recepient_Failed to save message into Firestore: \(error)")
                return
            }
            print("recepient_successfully saved message into Firestore")
        }
    }
    
}




