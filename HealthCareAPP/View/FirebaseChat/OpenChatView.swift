//
//  OpenChatView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 28/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift
import PhotosUI

struct OpenChatView: View {
    @State  var avatarImage = UIImage(named: "person")!
    @State  var isShowingPhotoPicker = false
    @State var imageSelected: Bool = false
    @StateObject var messagesManager = MessagesManager()
    let db = Firestore.firestore()
    @State  var message = ""
    @State var chatUserId: String = ""
    @State var  chatUserName : String = ""
    @State var chatUserAvatarImage  = UIImage(named: "person")!
    @State  var chatUserImage = ""
    @State var currentUserId: String = ""
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack(spacing: 5) {
                            let dataDecoded : Data = Data(base64Encoded: chatUserImage, options: .ignoreUnknownCharacters)!
                            let decodedimage = UIImage(data: dataDecoded)
                            
                            Image(uiImage: decodedimage ?? chatUserAvatarImage)
                                .resizable().clipShape(Circle())
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 30,height: 30)
                                .padding(.all, 5)
                            
                            Text(chatUserName)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
        VStack {
            if(messagesManager.chatMessages.isEmpty){
                Spacer()
                ShowEmptyView(animationName: "chat", description: Constants.lets_start_conversation)
                Spacer()
            }else{
                ScrollViewReader { proxy in
                    ScrollView {
                        ForEach(messagesManager.chatMessages, id: \.id) { message in
                            MessageBubble(message: message,currentUserId: currentUserId)
                        }
                    }.onAppear(perform: {
                        withAnimation {
                            proxy.scrollTo(messagesManager.chatMessages.last!.id, anchor: .bottom)
                        }
                    })
                    .padding(.top, 10)
                    .background(.white)
                    .onChange(of: messagesManager.lastMessageId) { id in
                        // When the lastMessageId changes, scroll to the bottom of the conversation
                        withAnimation {
                            proxy.scrollTo(id, anchor: .bottom)
                        }
                    }
                }
            }
            HStack {
                // this has to be copy
            
                Button {
                   isShowingPhotoPicker = true
                } label: {
                    Image(systemName: "plus.circle").resizable().frame(width: 35, height: 35, alignment: .center).foregroundColor(.gray)
                }

                // Custom text field created below
                CustomTextField(placeholder: Text(Constants.enter_message), text: $message)
                    .frame(height: 52)
                    .disableAutocorrection(true)
                Button {
                    if message.isEmpty{
                    }else{
                        messagesManager.sendMessage(fromId:currentUserId, toID:chatUserId, textMessage: message,uploadImg: "")
                        message = ""}
                } label: {
                    Image(systemName: "paperplane.fill")
                        .foregroundColor(.white)
                        .padding(10)
                        .background(Constants.primaryColor)
                        .cornerRadius(50)
                }
            }
            .padding(.horizontal)
            .padding(.vertical, 10)
            .background(Constants.grayColor)
            .cornerRadius(50)
            .padding()
        }
        .sheet(isPresented: $isShowingPhotoPicker, content: {
           PhotoPicker(avatarImage: $avatarImage,imageSelected: $imageSelected)
            
        })
        }.overlay{
            if(imageSelected){
                UploadImageView(currentUserId: currentUserId, chatUserId: chatUserId, avatarImage: $avatarImage, message: $message,imageSelected: $imageSelected)
            }
        }
        .accentColor(.white)
        .onAppear(perform:  getChatHistory)
    }
     
    private func getChatHistory(){
        self.currentUserId = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
        messagesManager.getMessages(fromId: currentUserId, toID: chatUserId)
    }
}

struct OpenChatView_Previews: PreviewProvider {
    static var previews: some View {
        OpenChatView(chatUserId: "",chatUserName: "", chatUserImage: "")
    }
}

struct CustomTextField: View {
    var placeholder: Text
    @Binding var text: String
    var editingChanged: (Bool)->() = { _ in }
    var commit: ()->() = { }
    
    var body: some View {
        ZStack(alignment: .leading) {
            // If text is empty, show the placeholder on top of the TextField
            if text.isEmpty {
                placeholder
                    .opacity(0.5)
            }
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit).accentColor(.blue)
        }
    }
}

struct UploadImageView: View {
    @State var currentUserId: String
    @State var chatUserId: String
    @Binding var avatarImage: UIImage
    @Binding var message: String
    @StateObject var messagesManager = MessagesManager()
    @Binding var imageSelected: Bool
        var body: some View {
        ZStack{}.onAppear {
            if(imageSelected){
                let imageData = avatarImage.jpegData(compressionQuality: 0.5)
                var imageBase64String = imageData?.base64EncodedString()
            messagesManager.sendMessage(fromId: currentUserId, toID: chatUserId, textMessage:message, uploadImg:  imageBase64String ?? "")
                imageBase64String = ""
            }else{
                messagesManager.sendMessage(fromId: currentUserId, toID: chatUserId, textMessage:message, uploadImg: "")
            }
            message = ""
            self.imageSelected = false
        }
}
}
