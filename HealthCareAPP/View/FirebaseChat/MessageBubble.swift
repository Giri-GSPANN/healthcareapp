//
//  MessageBubble.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 28/12/22.
//
import SwiftUI


struct MessageBubble: View {
    var message: ChatMessage
    var currentUserId: String
    @State var chatUserAvatarImage  =  UIImage(named: "person")!
    @State var showImage : Bool = false
    @State var showFullImaage: Bool = false
    var body: some View {
        ZStack{
        VStack(alignment: message.fromId.elementsEqual(currentUserId) ? .trailing : .leading) {
            if(message.textMessage.isEmpty){
                
            }else{
            HStack {
                Text(message.textMessage)
                    .padding()
                    .background(message.fromId.elementsEqual(currentUserId) ? Constants.primaryColor : Constants.grayColor)
                    .cornerRadius(10)
            }
            .frame(maxWidth: 300, alignment: message.fromId.elementsEqual(currentUserId) ? .trailing : .leading)
            }
            if(showImage){
                    Image(uiImage: chatUserAvatarImage)
                    .resizable().clipShape(Rectangle())
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 220,height: 220)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                                .stroke(Constants.grayColor, lineWidth: 0.5))
                                            .shadow(radius: 10)
                                            .padding(.all, 5).onTapGesture {
                                                showFullImaage.toggle()
                                            }
            }
        }.onAppear(perform: {
            getImageData()
        })
        .frame(maxWidth: .infinity, alignment: message.fromId.elementsEqual(currentUserId) ? .trailing : .leading)
        .padding(message.fromId.elementsEqual(currentUserId) ? .trailing : .leading)
        .padding(.horizontal, 10)
        }.sheet(isPresented: $showFullImaage) {
            showFullImagePopup(chatUserAvatarImage: $chatUserAvatarImage)
        }
    }
    
    func getImageData(){
        if (!message.uploaded_img.isEmpty){
            self.showImage = true
        if let dataDecoded : Data = Data(base64Encoded: message.uploaded_img, options: .ignoreUnknownCharacters) {
            if let decodedimage = UIImage(data: dataDecoded){
                chatUserAvatarImage = decodedimage
            }}
        }else{
            
        }
    }
}

struct MessageBubble_Previews: PreviewProvider {
    static var previews: some View {
        MessageBubble(message: ChatMessage(doucmentId: "", data: ["":""]),currentUserId: "")
    }
}

