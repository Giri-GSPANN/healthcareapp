//
//  MyPatientView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 05/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct MyPatientView: View {
    
    @State private var searchText = ""
    @ObservedObject var userArray = GetUserList()
    
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("Test")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.myPatients)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                } //toolbar
            
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                List {
                    ForEach(searchResults, id: \.self) { data in
                        
                        let username = data.firstName + " " + data.lastName
                        
                        NavigationLink(destination: PatientDetailsView(userId: data.userID), label: {
                            PatientCard(patientId: data.userID,image: data.photo, name: username, phoneNumber: data.phoneNumber, text: "Patient name")
                        })
                    }
                    .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                    .listRowSeparator(.hidden)
                    .listRowBackground(Constants.grayColor)
                }
                .listStyle(.plain)
                .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always))
                
                //Add Patient Button
                NavigationLink(destination: AddPatientView()) {
                    Text(Constants.addPatient)
                        .foregroundColor(Constants.whiteColor)
                        .fontWeight(.semibold)
                        .font(Font.system(size:Constants.font24))
                        .padding(Constants.padding3)
                } .padding(.all, 5)
                    .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
            }.onAppear {
                userArray.getAppointmentList(userType: "doctorID")
            }
            VStack{
                if(searchResults.isEmpty){
                    ShowEmptyView(animationName: "nodata", description: "My Patients Data Not Available !")
                    Spacer(minLength: 150)
                }
            }
        }.accentColor(.white)
    }
    var searchResults: [UserInfo] {
        if searchText.isEmpty {
            return userArray.userInfoList
        } else {
            return userArray.userInfoList.filter { $0.firstName.localizedCaseInsensitiveContains(searchText) }
        }
    }
}
