//
//  MedicalRecordSingleView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 04/01/23.
//

import SwiftUI

struct MedicalRecordSingleView: View {
    @State var chatUserAvatarImage = UIImage(named: "person")!
    var medicalRecord: MedicalRecord
    @State var showFullImaage: Bool = false
    var body: some View {
        ZStack{
            if(medicalRecord.uploaded_img.isEmpty && medicalRecord.uploaded_img.elementsEqual("")){}else{
             Image(uiImage: chatUserAvatarImage)
            .resizable().clipShape(Rectangle())
            .aspectRatio(contentMode: .fill)
            .frame(width: 200,height: 200)
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Constants.grayColor, lineWidth: 0.5))
                                    .shadow(radius:5)
                                    .padding(.all, 5).onTapGesture {
                                        showFullImaage.toggle()
                                    }
            }
        }.onAppear(perform: getImageData)
        .sheet(isPresented: $showFullImaage) {
            showFullImagePopup(chatUserAvatarImage: $chatUserAvatarImage)
        }
    }
    
    func getImageData(){
        if let dataDecoded : Data = Data(base64Encoded: medicalRecord.uploaded_img, options: .ignoreUnknownCharacters) {
            if let decodedimage = UIImage(data: dataDecoded){
                chatUserAvatarImage = decodedimage
            }}
        
    }
}
