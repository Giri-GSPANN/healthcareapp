//
//  showFullImagePopup.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 04/01/23.
//

import SwiftUI

struct showFullImagePopup: View {
    @Binding var chatUserAvatarImage: UIImage
    @Environment(\.presentationMode) var presentationMode
  
    var body: some View {
        Image(uiImage: chatUserAvatarImage).resizable().aspectRatio(contentMode: .fill).overlay(
            HStack{
                Image(systemName: "xmark.circle").font(.largeTitle).foregroundColor(.white).background(Constants.primaryColor).cornerRadius(5).padding(15)
            }.padding(.trailing,320).onTapGesture(perform: {
                presentationMode.wrappedValue.dismiss()
            })
           , alignment: .top
        )
    }
}
