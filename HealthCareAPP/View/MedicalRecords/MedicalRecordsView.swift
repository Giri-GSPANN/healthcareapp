//
//  MedicalRecordsView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 04/01/23.
//

import SwiftUI
import FirebaseFirestore

struct MedicalRecordsView: View {
    
    @StateObject var messagesManager = MessagesManager()
    let db = Firestore.firestore()
  
    
    // Create 2 Columns in grid, Flexible means take respected space in the phone view
    let layout = [GridItem(.flexible()),
                  GridItem(.flexible())]
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack(spacing: 2) {
                            Text(Constants.medical_records)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
        VStack {
            if(messagesManager.medicalRecords.isEmpty ){
                ShowEmptyView(animationName: "nodata",description: "You did not uploaded any medical documents yet!")
                Spacer(minLength: 150)
               }else{
                    ScrollView {
                        LazyVGrid(columns: layout,spacing: 5) {
                        ForEach(messagesManager.medicalRecords, id: \.id) { record in
                            MedicalRecordSingleView(medicalRecord: record)
                        }
                    }
                    }.padding(.all,10)
                }
    }
        }.accentColor(.white)
            .onAppear(perform:  getMedicalRecords)
    }
    
    func getMedicalRecords(){
        let currentUserId = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
        messagesManager.getMedicalRecords(userId: currentUserId)
    }
}

struct MedicalRecordsView_Previews: PreviewProvider {
    static var previews: some View {
        MedicalRecordsView()
    }
}
    

