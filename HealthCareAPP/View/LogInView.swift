//
//  LogInView.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 18/11/22.
//

import SwiftUI
import FirebaseCore
import FirebaseFirestore
import IQKeyboardManagerSwift
import FirebaseAuth
import FirebaseFirestoreSwift

struct LogInView: View {
    private let db = Firestore.firestore()
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var isHighlight = false
    @State private var isClcikOnRegister :Bool = false
    @State private var showAlert: Bool = false
    @State private var message: String = ""
    @State private var isLogInSuccessful: Bool = false
    @State private var isActivityIndicatorVisible: Bool = false
    @State private var userId: String = ""
    @State private var isOpenDoctorDashboard: Bool = false
    @State private var username = ""
    
   var body: some View {
        ZStack{
            Constants.loginBackGroundColor.edgesIgnoringSafeArea(.all)
            VStack(alignment: .center){
                
                Text(Constants.welcome).foregroundColor(Constants.blackColor).fontWeight(.semibold).font(Font.system(size: Constants.font40))
                
                Text(Constants.signin).foregroundColor(.gray).fontWeight(.semibold).font(Font.system(size: Constants.font18))
                
                
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.grayColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress,autocapitalizationType: .none).padding([.vertical], Constants.padding5)
                        
                        HealthCareSecureField(placeHolder: Constants.password,field: $password,fontSize :Constants.font18, backgroundColor: Constants.whiteColor, textColor: Constants.blackColor, icon: "lock.fill", iconColor: Constants.grayColor,strokeColor: Constants.grayColor, isHighlighted: $isHighlight)
                            .padding([.vertical], Constants.padding5)
                        
                        HStack(){
                            Spacer(minLength: 110)
                            
                            VStack{
                                Text(Constants.forgotPassword).foregroundColor(.gray).fontWeight(.semibold).font(Font.system(size: Constants.font18)).padding([.horizontal], Constants.padding20).overlay(
                                    NavigationLink("", destination: ForgotPasswordView()).opacity(0.0)
                                )
//                                Divider().padding([.horizontal], 20)
                            }
                        }
                        
                        ZStack{
                            HStack(){
                                NavigationLink(destination: getDestination(),isActive: $isLogInSuccessful) {
                                    Button(Constants.logIn) {
                                        if email.isEmpty  {
                                            message = Constants.emptyEmail
                                            showAlert = true
                                        } else{
                                            if Validation.isValidEmail(email: email){
                                                if password.isEmpty {
                                                    message = Constants.emptyPassword
                                                    showAlert = true
                                                }
                                                else if Validation.isValidPassword(password: password){
                                                    showAlert = false
                                                    logInUser()
                                                }
                                                else{
                                                    showAlert = true
                                                    message = Constants.validPassword
                                                }
                                            }
                                            else{
                                                showAlert = true
                                                message = Constants.validEmail
                                            }
                                        }
                                    }
                                    .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                                    .padding([.horizontal,.vertical], Constants.padding20)
                                    .alert(isPresented: $showAlert) {
                                        Alert(
                                            title: Text(Constants.appTitle),
                                            message: Text(message),
                                            dismissButton: .default(Text("Got it!"))
                                        )
                                    }
                                }.padding([.trailing], -32.0)
                                    .padding([.leading], -20.0)
                            }
                        }
                        
                        HStack(){
                            Text(Constants.donthaveanaccount).foregroundColor(Constants.grayColor).fontWeight(.semibold).font(Font.system(size: Constants.font14))
                            
                            
                            NavigationLink(destination: RegistrationView()) {
                                Text(Constants.registerHere).foregroundColor(Constants.blackColor).fontWeight(.semibold).font(Font.system(size: Constants.font18)).padding(Constants.padding3)
                            } .padding([.trailing], -32.0)
                                .padding([.leading], 50.0)
                        }.padding([.vertical],Constants.padding5)
                    }
                    .listRowSeparator(.hidden)
                }.frame(maxWidth: .infinity, maxHeight: 600,alignment: .center)
            }.progressDialog(isShowing: $isActivityIndicatorVisible, message: "Loading...")
        }  .navigationBarBackButtonHidden(true)
    }
    
    private func logInUser(){
        isActivityIndicatorVisible = true
        print("Login detail : \(email) :: \(password)")
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            DispatchQueue.main.async {
                isActivityIndicatorVisible = false
                if let e = error
                {
                    isLogInSuccessful = false
                    showAlert = true
                    message = e.localizedDescription
                    print("LogIn Error : \(e)")
                }
                else
                {
                    guard let userID = authResult?.user.uid else{
                        fatalError("Unable to get the userId")
                    }
                    userId = userID
                    
                    guard let email = authResult?.user.email else{
                        fatalError("Unable to get the userId")
                    }
                    
                    if let index = email.firstIndex(of: "@") {
                        username = String(email.prefix(upTo: index))
                    }
                    getUserDetail(id: userID)
                }
            }
        }
    }
    
    private func getUserDetail(id: String){
        let docRef = db.collection(Constants.FStore.userFBCollection).document(id)
        
        docRef.getDocument(as: UserInfo.self) { result in
            switch result {
            case .success(let userInfo):
                if(userInfo.registeredAs == Constants.doctor){
                    isOpenDoctorDashboard = true
                }else{
                    isOpenDoctorDashboard = false
                }
                isLogInSuccessful = true
                email = ""
                password = ""
                
                UserDefaults.standard.set(true, forKey: Constants.UserDefaultKey.isLogInSuccessful)
                UserDefaults.standard.set(userInfo.registeredAs, forKey: Constants.UserDefaultKey.registeredAs)
                UserDefaults.standard.set(userInfo.userID, forKey: Constants.UserDefaultKey.loggedInUserId)
                UserDefaults.standard.set(userInfo.firstName, forKey: Constants.UserDefaultKey.userFirstName)
                
            case .failure(let error):
                print("Error decoding userInfo: \(error)")
            }
        }
    }
    
    func getDestination() -> AnyView{
        if(isOpenDoctorDashboard){
            return AnyView( DoctorDashboardView())}else{
                return AnyView(PatientDashboardView())
            }
    }
}

struct LogInView_Previews: PreviewProvider {
    static var previews: some View {
        LogInView()
    }
}

