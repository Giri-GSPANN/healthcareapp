//
//  SearchView.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 01/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct SearchView: View {
    
    @State private var searchText = ""
    @StateObject var userArray = DoctorList()
    private let db = Firestore.firestore()
    
    var body: some View {
        ZStack{
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.search_doctor)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                } //toolbar
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                List {
                    
                    ForEach(searchResults, id: \.self) { data in
                        let username = data.firstName + " " + data.lastName
                        
                        NavigationLink(destination: DoctorDetailsView(userId: data.userID,showEditOption:false ), label: {
                            DoctorCard(doctorId:data.userID, image: data.photo, title: username, type: data.speclistist,address: data.address, price: Double(data.fee) ?? 0.0)
                        })
                        
                    }
                    .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                    .listRowSeparator(.hidden)
                    .listRowBackground(Constants.grayColor)
                }
                .listStyle(.plain)
                .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always))
                VStack{
                    if(searchResults.isEmpty){
                        ShowEmptyView(animationName: "apt", description: "Search Results Not Found!!")
                        Spacer(minLength: 150)
                    }
                }
            }
        }
    }
    
    var searchResults: [UserInfo] {
        if searchText.isEmpty {
            return userArray.list
        } else {
            return userArray.list.filter {
                let username = $0.firstName + $0.lastName
                return username.localizedCaseInsensitiveContains(searchText) }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}



