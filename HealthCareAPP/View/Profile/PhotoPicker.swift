//
//  PhotoPicker.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 06/12/22.
//

// Used to select the photos from device/simulator

import SwiftUI

struct PhotoPicker: UIViewControllerRepresentable {
    
    @Binding var avatarImage: UIImage
    @Binding var imageSelected: Bool
    
    func makeUIViewController(context: Context) -> some UIImagePickerController {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(photoPicker: self, imageSelected: $imageSelected)
    }
    
    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
        
        let photoPicker: PhotoPicker
        @Binding var imageSelected: Bool
        
        init(photoPicker: PhotoPicker, imageSelected: Binding<Bool>) {
            self.photoPicker = photoPicker
            _imageSelected = imageSelected
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.editedImage] as? UIImage{
                photoPicker.avatarImage = image
                imageSelected = true
            }else{
                imageSelected = false
                return
            }
            picker.dismiss(animated: true)
        }
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            imageSelected = false
            picker.dismiss(animated: true)
        }
    }
}


