//
//  DoctorDetailsView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 12/12/22.
//

import SwiftUI
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift

struct DoctorDetailsView: View {
    @State private var firstName : String = ""
    @State private var lastName  : String = ""
    @State private var email  : String = ""
    @State private var phoneNumber  : String = ""
    @State private var address  : String = ""
    @State private var selectedGender = Constants.male
    @State private var selectedSpecialist = Constants.specialistArray[0]
    @State private var age  : String = ""
    @State private var fee  : String = ""
    @State private var about  : String = ""
    @State private var experiance  : String = ""
    @State private var selectedSpecialistLabel : String  = ""
    @State private var  gotoDocEditScreen : Bool = false
    @State private var isActivityIndicatorVisible: Bool = false
    @State private var isHighlight = false
    @State private var image  : String = ""
    @State private var avatarImage = UIImage(named: "person")!
    private let db = Firestore.firestore()
    var userId : String = ""
    var showEditOption : Bool = false
    var patientID =  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
    
    var body: some View {
        ZStack {
            if showEditOption {
                Text("")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        
                        ToolbarItem(placement: .principal) {
                            HStack {
                                Text(Constants.profile)
                                    .fontWeight(.regular)
                                    .font(Font.system(size: Constants.font24))
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        }
                        
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button(action: {
                                self.gotoDocEditScreen = true
                            }, label: {
                                Image(systemName: "pencil").foregroundColor(.white)
                            })
                        }
                    }
            }else{
                Text("")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        ToolbarItem(placement: .principal) {
                            HStack {
                                Text(Constants.profile)
                                    .fontWeight(.regular)
                                    .font(Font.system(size: Constants.font24))
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        }
                    }
            }
            VStack{
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        
                        HStack() {
                            Spacer()
                            
                            Image(uiImage: avatarImage).resizable().clipShape(Circle()).frame(width: 120, height: 120)
                            
                            Spacer()
                        }
                        HStack(spacing: 20) {
                            HealthcareTextView(placeHolder: Constants.gender, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor,isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Gender", selection: $selectedGender) {
                                ForEach(Constants.genderArray, id: \.self) {
                                    Text($0)
                                }
                            }.pickerStyle(.segmented).disabled(true)
                        }
                        
                        HealthCareTextField(placeHolder: Constants.firstName,field: $firstName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.lastName,field: $lastName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.phoneNumber,field: $phoneNumber, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "phone.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HStack(spacing : -30) {
                            HealthcareTextView(placeHolder: Constants.specialist, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("", selection: $selectedSpecialist) {
                                ForEach(Constants.specialistArray, id: \.self) { value in
                                    Text(value)
                                }
                            }.pickerStyle(.automatic).disabled(true)
                        }
                        
                        HealthCareTextField(placeHolder: Constants.fees,field: $fee, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "indianrupeesign.circle.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.about,field: $about, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "staroflife.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.address,field: $address, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                    }
                }
                if !showEditOption {
                    ZStack{
                        HStack(){
                            NavigationLink(destination: DoctorAvailabilityView(userId: userId, userName: firstName+" "+lastName, patientId: patientID, actualStatus: AppointmentStatus.Pending.rawValue)) {
                                Text(Constants.checkAvailability)
                            }
                            .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                            .padding([.horizontal], Constants.padding20)
                        }.padding(.top)
                    }
                }
                NavigationLink(destination: DoctorProfileEditView(userId : userId), isActive: self.$gotoDocEditScreen){Text("")}
            }
        }.onAppear(perform: getUserDetail)
    }
    
    private func getUserDetail()
    {
        var id : String
        if(userId.isEmpty){
            id =  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
        }
        else{
            id = userId
        }
        
        let docRef = db.collection(Constants.FStore.userFBCollection).document(id)
        
        docRef.getDocument(as: UserInfo.self) { response in
            switch response {
            case .success(let userInfo):
                firstName = userInfo.firstName
                lastName = userInfo.lastName
                email = userInfo.email
                phoneNumber = userInfo.phoneNumber
                selectedGender = userInfo.gender
                image = userInfo.photo
                address = userInfo.address
                about = userInfo.about
                fee = userInfo.fee
                selectedSpecialist = userInfo.speclistist
                
                if let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters) {
                    if let decodedimage = UIImage(data: dataDecoded){
                        avatarImage = decodedimage
                    }}
                
            case .failure(let error):
                print("Error decoding userInfo: \(error)")
            }
        }
    }
}

struct DoctorDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorDetailsView(userId: "", showEditOption : false)
    }
}
