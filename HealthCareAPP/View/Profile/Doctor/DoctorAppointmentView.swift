//
//  DoctorAppointmentView.swift
//  HealthCareAPP
//
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct DoctorAppointmentView: View {
    
    // Selected Date
    @State private var selectedDate = Date()
    // Default Date
    @State private var todayDate = Date()
    @State private var searchText = ""
    @ObservedObject var getCalenderAppointmentsList = GetCalenderAppointmentsList(doctorID: "", date: "")
    @State private var appointmentTime:String = ""
    @State private var isLoading = false
    @Environment(\.presentationMode) var presentation
    private let db = Firestore.firestore()
    let userId = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
    
    //Date formate which is used to send to the server
    var dateFormatter: DateFormatter{
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }
    
    init(){
        getCalenderAppointmentsList(date: CommonFunctionUtils.getTodayDate(format: "MM/dd/YY"))
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.automatic)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.myCalendar)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                HStack(alignment:.center){
                    if(CommonFunctionUtils.getTodayDate(format: "MM/dd/YY") == dateFormatter.string(from: selectedDate)){}else{
                        //Previous day button
                        Image(systemName:"chevron.left").onTapGesture {
                            let calendar = Calendar.current
                            selectedDate = calendar.date(byAdding: .day, value: -1, to: selectedDate)!
                        }.padding()
                    }
                    
                    Spacer()
                    
                    Image(systemName: "calendar.circle.fill").resizable().foregroundColor(.white).frame(width: 30, height:30, alignment: .center)
                    // Date picker
                    DatePicker("", selection: $selectedDate, in: Date()..., displayedComponents: .date).padding(.all,-15).labelsHidden().id(selectedDate).colorInvert().font(Font.headline).disabled(true).onChange(of: selectedDate) {
                        print(dateFormatter.string(from: selectedDate))
                        print($0)
                        getCalenderAppointmentsList(date : dateFormatter.string(from: selectedDate))
                    }
                    
                    Spacer()
                    
                    //Next Day button
                    Image(systemName:"chevron.right").onTapGesture {
                        let calendar = Calendar.current
                        selectedDate = calendar.date(byAdding: .day, value: 1, to: selectedDate)!
                    }.padding()
                }.background(Constants.primaryColor).foregroundColor(.white).font(.title)
                
                ZStack{
                    VStack{
                        if(searchResults.isEmpty){
                            ShowEmptyView(animationName: "apt", description: "Appointments Are Not Available On This Date!")                        }
                        else
                        {
                            List {
                                ForEach(searchResults, id: \.self) { customAppointmentModel in
                                    PatientAppointmentViewCard(time: customAppointmentModel.appointment.time, appointmentID: customAppointmentModel.appointment.appointmentID, patientName: customAppointmentModel.userName, phoneNumber: customAppointmentModel.phoneNumber, email: customAppointmentModel.email, patientId: customAppointmentModel.appointment.patientID, image: customAppointmentModel.image)
                                }
                                .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                                .listRowSeparator(.hidden)
                                .listRowBackground(Constants.grayColor)
                            }
                            .listStyle(.plain)
                        }
                    }
                }
            }
        }.progressDialog(isShowing: $isLoading, message: "Loading...")
    }
    var searchResults: [CustomAppointmentModel] {
        if searchText.isEmpty {
            return getCalenderAppointmentsList.patientAppointmentModelList
        } else {
            return getCalenderAppointmentsList.patientAppointmentModelList
        }
    }
    
    private func goBack(){
        self.presentation.wrappedValue.dismiss()
    }
    
    private func hideLoader(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
            isLoading = false
        }
    }
    
    private func getCalenderAppointmentsList(date: String){
        getCalenderAppointmentsList.list.removeAll()
        getCalenderAppointmentsList.patientAppointmentModelList.removeAll()
        getCalenderAppointmentsList.fetchData(doctorID: self.userId!, date: date)
    }
}

struct DoctorAppointmentView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorAppointmentView()
    }
}

