//
//  DoctorAvailabilityView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 14/12/22.
//
// Mark: -
// 1.DoctorAvailabilityView will call from the Search Doctor --> Doctor Details page where he will click on Check Availability Button
// 2.Get DoctorTimings Randomly from the DoctorTimingsViewModel

import SwiftUI
import FirebaseAuth
import FirebaseFirestore

struct DoctorAvailabilityView: View {
    
    // Selected Date
    @State private var selectedDate = Date()
    
    // Default Date
    @State private var todayDate = Date()
    
    // ObservedObject: Observes changes from its ViewModel
    @StateObject var doctorTimingsVM = DoctorTimingsViewModel()
    
    @ObservedObject var appointmentList = AppointmentList(doctorID: "", date: "")
    
    //Alert Popup
    @State private var showBookingAlert: Bool = false
    @State private var showBookingFullAlert: Bool = false
    @State private var appointmentTime:String = ""
    @State private var isLoading = false
    @State private var isConfirmBookingAlert: Bool = false
    @Environment(\.presentationMode) var presentation
    private let db = Firestore.firestore()
    var userId : String = ""
    var userName : String = ""
    var patientId : String = ""
    var appointmentActualStatus : String = ""
    
    //Date formate which is used to send to the server
    var dateFormatter: DateFormatter{
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }
    
    // Create 3 Columns in grid, Flexible means take respected space in the phone view
    let layout = [GridItem(.flexible()),
                  GridItem(.flexible()),
                  GridItem(.flexible())]
    
    init(userId: String , userName: String, patientId: String , actualStatus: String){
        self.userId = userId
        self.userName = userName
        self.patientId = patientId
        self.appointmentActualStatus = actualStatus
        getAppointmentList(date: CommonFunctionUtils.getTodayDate(format: "MM/dd/YY"))
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.automatic)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.appointment)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }//toolbar
            VStack{
                HStack(alignment:.center){
                    if CommonFunctionUtils.getTodayDate(format: "MM/dd/YY").compare(dateFormatter.string(from: selectedDate)) == .orderedSame{}else{
                        //Previous day button
                        Image(systemName:"chevron.left").onTapGesture {
                            let calendar = Calendar.current
                            selectedDate = calendar.date(byAdding: .day, value: -1, to: selectedDate)!
                        }.padding()
                    }
                    Spacer()
                    
                    Image(systemName: "calendar.circle.fill").resizable().foregroundColor(.white).frame(width: 30, height:30, alignment: .center)
                    // Date picker
                    DatePicker("", selection: $selectedDate, in: Date()..., displayedComponents: .date).padding(.all,-15).labelsHidden().id(selectedDate).colorInvert().font(Font.headline).disabled(true).onChange(of: selectedDate) {
                        print(dateFormatter.string(from: selectedDate))
                        print($0)
                        ///Here we will change the list
                        isLoading = true
                        doctorTimingsVM.clearData()
                        doctorTimingsVM.appendData(date:  dateFormatter.string(from: selectedDate))
                        getAppointmentList(date: dateFormatter.string(from: selectedDate))
                        updateList()
                        
                    }
                    
                    Spacer()
                    //Next Day button
                    Image(systemName:"chevron.right").onTapGesture {
                        let calendar = Calendar.current
                        selectedDate = calendar.date(byAdding: .day, value: 1, to: selectedDate)!
                    }.padding()
                }.background(Constants.primaryColor).foregroundColor(.white).font(.title)
                    .alert(isPresented: $isConfirmBookingAlert) {
                        return confirmBookingAlert()
                    }
                
                ScrollView {
                    LazyVGrid(columns:layout) {
                        ForEach(doctorTimingsVM.doctorTimingsArray){ item in
                            TimingViewCard(aptmt_time: item.aptmt_time, aptmt_status: item.aptmt_status)
                                .frame(width:110,height: 100).background(
                                    item.aptmt_status.elementsEqual(AppointmentStatus.Available.rawValue) ? Constants.primaryColor : .red).cornerRadius(10)
                                .onTapGesture {
                                    DispatchQueue.main.async {
                                        print(item.aptmt_status)
                                        print(item.aptmt_time)
                                        appointmentTime = item.aptmt_time
                                        if(item.aptmt_status.elementsEqual(AppointmentStatus.Available.rawValue)){
                                            showBookingAlert = true
                                        }
                                    }
                                }
                                .alert(isPresented: $showBookingAlert) {
                                    return getBookingAlert(bookingMessage: "Your booking an appointment with Dr. \(userName) on \(dateFormatter.string(from: selectedDate)) at \(appointmentTime) So would you like to confirm?",data: item)
                                }
                        }
                    }.padding([.leading,.trailing])
                }
            }
        }.progressDialog(isShowing: $isLoading, message: "Loading...")
            .onAppear(perform:   updateList)
    }
    
    //Show Booking Alert
    func getBookingAlert(bookingMessage: String, data : DoctorTimings) -> Alert{
        return Alert(
            title: Text(Constants.bookingAppointment),
            message: Text(bookingMessage),
            primaryButton: .cancel(Text("No")),
            secondaryButton: .default(
                Text("Yes, Book Appointment"),action: {
                    bookAppointment(data: data)
                    isLoading = true
                })
        )
    }
    
    //Show Confirm Booking Alert
    private func confirmBookingAlert() -> Alert{
        return Alert(
            title: Text(Constants.bookingAppointment),
            message: Text(Constants.confirmBookAppointmentMessage),
            dismissButton: .default(Text("Ok"), action: self.goBack))
    }
    
    private func goBack(){
        self.presentation.wrappedValue.dismiss()
    }
    
    private func getAppointmentList(date: String){
        appointmentList.list.removeAll()
        appointmentList.fetchData(doctorID: self.userId, date: date)
    }
    
    private func bookAppointment(data : DoctorTimings){
        var ref: DocumentReference? = nil
        
        ref = db.collection(Constants.FStore.appointmentFBCollection).document()
        
        let appointmentData =  Appointment(appointmentID: ref!.documentID, patientID: patientId, doctorID: userId, date: dateFormatter.string(from: selectedDate), time: appointmentTime, status: AppointmentStatus.Booked.rawValue, actualStatus: appointmentActualStatus)
        
        ref!.setData(appointmentData.dictionary){
            (error) in
            if let e = error {
                print("There was an issue saving data to firestore, \(e)")
            } else {
                isLoading = false
                if let row = doctorTimingsVM.doctorTimingsArray.firstIndex(where: {$0.aptmt_date + $0.aptmt_time == dateFormatter.string(from: selectedDate)+appointmentTime }) {
                    doctorTimingsVM.doctorTimingsArray[row].aptmt_status = appointmentData.status
                }
                isConfirmBookingAlert = true
                print("Appointment Created Successfully.")
            }
        }
    }
    
    private func updateList(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1){
            for item in appointmentList.list{
                var result = doctorTimingsVM.doctorTimingsArray.first{
                    return $0.aptmt_date + $0.aptmt_time == item.date+item.time
                }
                result?.aptmt_status = item.status
                
                if let row = doctorTimingsVM.doctorTimingsArray.firstIndex(where: {$0.aptmt_date + $0.aptmt_time == item.date+item.time }) {
                    doctorTimingsVM.doctorTimingsArray[row] = result!
                }
            }
            isLoading = false
        }
    }
}

struct DoctorAvailabilityView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorAvailabilityView(userId: "", userName: "" , patientId: "", actualStatus: AppointmentStatus.Pending.rawValue)
    }
}

