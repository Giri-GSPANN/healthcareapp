//
//  PatientDetailsView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 12/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct PatientDetailsView: View {
    
    @State private var firstName : String = ""
    @State private var lastName  : String = ""
    @State private var email  : String = ""
    @State private var phoneNumber  : String = ""
    @State private var address  : String = ""
    @State private var selectedGender = Constants.male
    @State private var age  : String = ""
    @State private var gotoPatientEditScreen : Bool = false
    @State private var isHighlight = false
    @State private var image = ""
    @State private var avatarImage = UIImage(named: "person")!
    private let db = Firestore.firestore()
    var userId : String = ""
    var showEditOption : Bool = false
    
    var body: some View {
        ZStack {
            if showEditOption {
                Text("")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        
                        ToolbarItem(placement: .principal) {
                            HStack {
                                Text(Constants.profile)
                                    .fontWeight(.regular)
                                    .font(Font.system(size: Constants.font24))
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        }
                        
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button(action: {
                                self.gotoPatientEditScreen = true
                            }, label: {
                                Image(systemName: "pencil").foregroundColor(.white)
                            })
                        }
                    }
            }else{
                Text("")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        
                        ToolbarItem(placement: .principal) {
                            HStack {
                                Text(Constants.profile)
                                    .fontWeight(.regular)
                                    .font(Font.system(size: Constants.font24))
                                    .foregroundColor(.white)
                                    .bold()
                            }
                        }
                    }
            }
            
            VStack{
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        
                        HStack() {
                            
                            Spacer()
                            
                            Image(uiImage: avatarImage).resizable().clipShape(Circle()).frame(width: 120, height: 120)
                            
                            Spacer()
                        }
                        
                        HStack(spacing: 20) {
                            HealthcareTextView(placeHolder: Constants.gender, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor,isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Gender", selection: $selectedGender) {
                                ForEach(Constants.genderArray, id: \.self) {
                                    Text($0)
                                }
                            }.pickerStyle(.segmented).disabled(true)
                        }
                        
                        HealthCareTextField(placeHolder: Constants.firstName,field: $firstName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.lastName,field: $lastName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.phoneNumber,field: $phoneNumber, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "phone.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad, isDisable: true).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.age,field: $age, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "figure.stand", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .phonePad, isDisable: true).padding([ .vertical], Constants.padding5)
                    }
                }
                NavigationLink(destination: PatientProfileEditView(userId : userId), isActive: self.$gotoPatientEditScreen){Text("")}
            }
            .navigationBarTitleDisplayMode(.inline)
        }.onAppear(perform: getUserDetail)
    }
    
    private func getUserDetail()
    {
        var id : String
        if(userId.isEmpty){
            id =  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
        }
        else{
            id = userId
        }
        
        let docRef = db.collection(Constants.FStore.userFBCollection).document(id)
        docRef.getDocument(as: UserInfo.self) { response in
            switch response {
                
            case .success(let userInfo):
                firstName = userInfo.firstName
                lastName = userInfo.lastName
                email = userInfo.email
                phoneNumber = userInfo.phoneNumber
                selectedGender = userInfo.gender
                image = userInfo.photo
                age = userInfo.age
                
                if let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters) {
                    if let decodedimage = UIImage(data: dataDecoded){
                        avatarImage = decodedimage
                    }}
                
            case .failure(let error):
                print("Error decoding userInfo: \(error)")
            }
        }
    }
}

struct PatientDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        PatientDetailsView(userId : "", showEditOption : false)
    }
}
