//
//  PatientAppointmentView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 16/12/22.
//
// MARK:- Doctor see this page from Doctor Dashboard on click of Appointment Button.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct PatientAppointmentView: View {
    
    @State private var searchText = ""
    @ObservedObject var getPatientAppointmentList = GetPatientAppointmentList()
    
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("Test")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.appointment)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                } //toolbar
            
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                List {
                    ForEach(searchResults, id: \.self) { customAppointmentModel in
                        DoctorAppointmentViewCard(date: customAppointmentModel.appointment.date, time: customAppointmentModel.appointment.time, appointmentID: customAppointmentModel.appointment.appointmentID, doctorName: customAppointmentModel.userName, phoneNumber: customAppointmentModel.phoneNumber, apptStatus: customAppointmentModel.appointment.actualStatus, email: customAppointmentModel.email, doctorId: customAppointmentModel.appointment.doctorID,image: customAppointmentModel.image)
                    }
                    .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                    .listRowSeparator(.hidden)
                    .listRowBackground(Constants.grayColor)
                }
                .listStyle(.plain)
            }.onAppear {
                getPatientAppointmentList.fetchData()
            }
            VStack{
                if(searchResults.isEmpty){
                    ShowEmptyView(animationName: "apt", description: "No Appointments Available!!")
                    Spacer(minLength: 150)
                }
            }
        }.accentColor(.white)
    }
    var searchResults: [CustomAppointmentModel] {
        let sortedList = getPatientAppointmentList.doctorAppointmentModelList.sorted{ $0.appointment.date.compare($1.appointment.date) == .orderedAscending}
        return sortedList
    }
}

struct PatientAppointmentView_Previews: PreviewProvider {
    static var previews: some View {
        PatientAppointmentView()
    }
}
