//
//  PatientProfileEditView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 05/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct PatientProfileEditView: View {
    @State private var registeredAs = ""
    @State private var firstName = ""
    @State private var lastName = ""
    @State private var email = ""
    @State private var phoneNumber = ""
    @State private var address = ""
    @State private var selectedGender = Constants.male
    @State private var fakeGender = ""
    @State private var age = ""
    @State private var message: String = ""
    @State private var showAlert: Bool = false
    @State private var isHighlight = false
    @State private var isShowingPhotoPicker = false
    @State private var isActivityIndicatorVisible: Bool = false
    @State private var avatarImage = UIImage(named: "person")!
    @State private var image = ""
    @State private var imageSelected = false
    private let db = Firestore.firestore()
    var userId : String = ""
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.editProfile)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                }
            VStack{
                Form{
                    Section( header: Text("").font(.headline.bold()).foregroundColor(.black)){
                        HStack() {
                            Spacer()
                            
                            Image(uiImage: avatarImage).resizable().clipShape(Circle()).frame(width: 120, height: 120).onTapGesture {
                                isShowingPhotoPicker = true
                            }
                            
                            Spacer()
                        }
                        
                        HStack(spacing: 20) {
                            HealthCareTextField(placeHolder: Constants.gender,field: $fakeGender, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,isDisable: true).padding([ .vertical], Constants.padding5)
                            
                            Picker("Gender", selection: $selectedGender) {
                                ForEach(Constants.genderArray, id: \.self) {
                                    Text($0)
                                }
                            }.pickerStyle(.segmented)
                        }
                        
                        HealthCareTextField(placeHolder: Constants.firstName,field: $firstName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.lastName,field: $lastName, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "person.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .default).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.email,field: $email, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.grayColor,icon: "envelope.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .emailAddress,autocapitalizationType: .none).padding([ .vertical], Constants.padding5).disabled(true)
                        
                        HealthCareTextField(placeHolder: Constants.phoneNumber,field: $phoneNumber, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "phone.fill", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad).padding([ .vertical], Constants.padding5)
                        
                        HealthCareTextField(placeHolder: Constants.age,field: $age, fontSize :Constants.font18, backgroundColor: Constants.whiteColor,textColor: Constants.blackColor,icon: "figure.stand", iconColor: Constants.grayColor, strokeColor: Constants.secondaryColor, isHighlighted: $isHighlight,keyBoardType: .numberPad).padding([ .vertical], Constants.padding5)
                    }
                }
                
                Spacer()
                ZStack{
                    HStack(){
                        Button(Constants.save) {
                            isValidateFiled()
                        }
                        .buttonStyle(HealthCareButton(fontSize :Constants.font24,backgroundColor: Constants.primaryColor,textColor: Constants.secondaryColor,strokeColor: Constants.secondaryColor))
                        .padding([.horizontal, .vertical], Constants.padding10)
                        .alert(isPresented: $showAlert) {
                            Alert(
                                title: Text(Constants.appTitle),
                                message: Text(message),
                                dismissButton: .default(Text("OK"))
                            )
                        }
                    }
                    //.padding(.top)
                }
                
            }.progressDialog(isShowing: $isActivityIndicatorVisible, message: "Loading...")
        }.onAppear(perform: getUserDetail)
            .sheet(isPresented: $isShowingPhotoPicker, content: {
                PhotoPicker(avatarImage: $avatarImage,imageSelected: $imageSelected)
            })
    }
    
    private func isValidateFiled()
    {
        if(firstName.isEmpty)
        {
            message = Constants.firstNameValidation
            showAlert = true
        }
        else
        {
            if(lastName.isEmpty){
                message = Constants.lastNameValidation
                showAlert = true
            }
            else
            {
                if phoneNumber.isEmpty {
                    message = Constants.emptyPhone
                    showAlert = true
                }else if !Validation.isValidPhone(phone: phoneNumber){
                    message = Constants.validPhoneNumber
                    showAlert = true
                }else if age.isEmpty {
                    message = Constants.ageEmpty
                    showAlert = true
                }else{
                    updateUser()
                }
            }
        }
    }
    
    private func getUserDetail()
    {
        var id : String
        if(userId.isEmpty){
            id =  UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)!
        }
        else{
            id = userId
        }
        
        let docRef = db.collection(Constants.FStore.userFBCollection).document(id)
        docRef.getDocument(as: UserInfo.self) { response in
            switch response {
                
            case .success(let userInfo):
                firstName = userInfo.firstName
                lastName = userInfo.lastName
                email = userInfo.email
                phoneNumber = userInfo.phoneNumber
                selectedGender = userInfo.gender
                image = userInfo.photo
                age = userInfo.age
                registeredAs = userInfo.registeredAs
                address = userInfo.address
                
                if let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters) {
                    if let decodedimage = UIImage(data: dataDecoded){
                        avatarImage = decodedimage
                    }}
                
            case .failure(let error):
                print("Error decoding userInfo: \(error)")
            }
        }
    }
    
    private func updateUser(){
        isActivityIndicatorVisible = true
        
        let imageData = avatarImage.jpegData(compressionQuality: 0.5)
        let imageBase64String = imageData?.base64EncodedString()
        
        let userID = UserDefaults.standard.string(forKey: Constants.UserDefaultKey.loggedInUserId)
        
        let userInfo = UserInfo(userID: userID!, registeredAs: registeredAs, firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber, photo: imageBase64String ?? "", gender: selectedGender, speclistist: "", about: "", fee: "", address: address, experience: "", age: age)
        
        db.collection(Constants.FStore.userFBCollection).document(userID!).setData(userInfo.dictionary){
            (error) in
            if let e = error {
                print("There was an issue saving data to firestore, \(e)")
            } else {
                print("Successfully saved data.")
                DispatchQueue.main.async {
                    presentation.wrappedValue.dismiss()
                }
            }
            isActivityIndicatorVisible = false
        }
    }
}

struct PatientProfileEditView_Previews: PreviewProvider {
    static var previews: some View {
        PatientProfileEditView(userId : "")
    }
}
