//
//  MyDoctorsView.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 27/12/22.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct MyDoctorsView: View {
    
    @State private var searchText = ""
    @ObservedObject var userArray = GetUserList()
    
    
    var body: some View {
        ZStack(alignment: .top) {
            Text("")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(Constants.myDoctors)
                                .fontWeight(.regular)
                                .font(Font.system(size: Constants.font24))
                                .foregroundColor(.white)
                                .bold()
                        }
                    }
                } //toolbar
            
            Constants.grayColor.edgesIgnoringSafeArea(.all)
            VStack{
                List {
                    ForEach(searchResults, id: \.self) { data in
                        let username = "Dr. " + data.firstName + " " + data.lastName
                        NavigationLink(destination: DoctorDetailsView(userId: data.userID), label: {
                            PatientCard(patientId:data.userID,image: data.photo, name: username, phoneNumber: data.phoneNumber, text: "Doctor Name")
                        })
                    }
                    .listRowInsets( EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5) )
                    .listRowSeparator(.hidden)
                    .listRowBackground(Constants.grayColor)
                }
                .listStyle(.plain)
                .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always))
            }.onAppear {
                userArray.getAppointmentList(userType: "patientID")
            }
            VStack{
                if(searchResults.isEmpty ){
                    ShowEmptyView(animationName: "apt", description: "Search Results For My Doctors Not Found!!")
                    Spacer(minLength: 150)
                }
            }
            
        }.accentColor(.white)
    }
    var searchResults: [UserInfo] {
        if searchText.isEmpty {
            return userArray.userInfoList
        } else {
            return userArray.userInfoList.filter { $0.firstName.localizedCaseInsensitiveContains(searchText) }
        }
    }
}
