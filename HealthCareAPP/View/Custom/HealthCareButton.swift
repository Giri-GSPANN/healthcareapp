//
//  HealthCareButton.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 21/11/22.
//

import SwiftUI

struct HealthCareButton: ButtonStyle {
    
    let fontSize : CGFloat
    let backgroundColor : Color
    let textColor : Color
    let strokeColor : Color
    let cornorRaduis :  CGFloat
    let width : CGFloat
    let height : CGFloat
    
    init(fontSize: CGFloat = 14, backgroundColor: Color = .blue, textColor:Color = .white, strokeColor : Color = .white, cornorRaduis :CGFloat = 10, width : CGFloat = .infinity, height : CGFloat = 50) {
        self.fontSize = fontSize
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.strokeColor = strokeColor
        self.cornorRaduis = cornorRaduis
        self.width = width
        self.height = height
    }
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(Font.system(size: fontSize))
            .frame(height: height)
            .frame(maxWidth: width)
            .background(RoundedRectangle(cornerRadius: cornorRaduis).fill(backgroundColor))
            .foregroundColor(textColor)
            .overlay(RoundedRectangle(cornerRadius: cornorRaduis).stroke(strokeColor))
    }
}
