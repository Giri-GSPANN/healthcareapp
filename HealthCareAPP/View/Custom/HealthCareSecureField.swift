//
//  HealthCareSecureField.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 21/11/22.
//

import SwiftUI

struct HealthCareSecureField: View {
    
    // Constants, so all "TextFields will be the same in the app"
    let placeHolder : String
    let fontSize : CGFloat
    let backgroundColor : Color
    let textColor : Color
    let icon : String?
    let iconColor : Color
    let strokeColor : Color
    let cornorRaduis :  CGFloat
   
    
    // The @State Object
    @Binding var field: String
    // A custom variable for a "TextField"
    @Binding var isHighlighted: Bool
    
    init(placeHolder : String = "", field: Binding<String>, fontSize: CGFloat = 14, backgroundColor: Color = .blue, textColor:Color = .white, icon: String, iconColor: Color = .gray, strokeColor : Color = .white, cornorRaduis :CGFloat = 10, isHighlighted: Binding<Bool>) {
        self.placeHolder  = placeHolder
        self._field = field
        self.fontSize = fontSize
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.icon = icon
        self.iconColor = iconColor
        self.strokeColor = strokeColor
        self.cornorRaduis = cornorRaduis
        self._isHighlighted = isHighlighted
    }
    
    var body: some View {
        HStack {
            if let imageIcon = icon{
                Image(systemName: imageIcon).foregroundColor(iconColor)
            }
            SecureField(placeHolder, text: $field)
            
        }.font(Font.system(size: fontSize))
            .padding()
            .background(RoundedRectangle(cornerRadius: cornorRaduis).fill(backgroundColor))
            .foregroundColor(textColor)
            .overlay(RoundedRectangle(cornerRadius: cornorRaduis).stroke(strokeColor))
    }
}
