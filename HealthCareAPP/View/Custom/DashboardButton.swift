//
//  DashboardButton.swift
//  HealthCareAPP
//
//  Created by Suhas Bachewar on 23/11/22.
//

import SwiftUI

struct DashboardButton: View {
    let buttonName : String
    let imageName : String
    let imgWidth : CGFloat
    let imgHeight : CGFloat
    let imgPadding : Edge.Set
    
    var body: some View {
        VStack {
            Image(systemName:imageName)
                .resizable()
                .foregroundColor(Constants.primaryColor)
                .padding(.bottom)
                .frame(width: imgWidth, height: imgHeight)
                .padding(imgPadding)
            
            Text(buttonName).padding(.bottom)
        }.frame(width: 160, height: Constants.fSize120)
            .background(Color(UIColor(white: 0.7, alpha: 0.1)))
            .foregroundColor(Constants.primaryColor)
            .cornerRadius(Constants.fSize20)
    }
    
}
