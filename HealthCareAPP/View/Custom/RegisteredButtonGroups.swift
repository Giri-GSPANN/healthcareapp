//
//  RadioButtonGroups.swift
//  HealthCareAPP
//
//  Created by Zakir Hussain on 24/11/22.
//

import SwiftUI

struct RegisteredButtonGroups: View {
    let callback: (String) -> ()
    
    @State var selectedId: String = ""
    
    var body: some View {
        HStack(alignment: .center, spacing: 0) {
            radioDoctor
            radioPatient
        }
    }
    
    var radioDoctor: some View {
        RadioButtonField(
            id: Constants.doctor,
            label: Constants.doctor,
            color: Constants.secondaryColor,
            isMarked: selectedId == Constants.doctor ? true : false,
            callback: radioGroupCallback
        )
    }
    
    var radioPatient: some View {
        RadioButtonField(
            id: Constants.patient,
            label: Constants.patient,
            color: Constants.secondaryColor,
            isMarked: selectedId == Constants.patient ? true : false,
            callback: radioGroupCallback
        )
    }
    
    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}
