//
//  HealthcareTextView.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 12/12/22.
//

import SwiftUI

struct HealthcareTextView: View {
    
    // Constants, so all "TextFields will be the same in the app"
    let placeHolder : String
    let fontSize : CGFloat
    let backgroundColor : Color
    let textColor : Color
    let icon : String?
    let iconColor : Color
    let strokeColor : Color
    let cornorRaduis :  CGFloat
    let keyBoardType : UIKeyboardType
    let isDisable : Bool
    let autocapitalizationType : UITextAutocapitalizationType
    init(placeHolder : String = "", fontSize: CGFloat = 14, backgroundColor: Color = .blue, textColor:Color = .white, icon: String, iconColor: Color = .gray, strokeColor : Color = .white, cornorRaduis :CGFloat = 10, keyBoardType : UIKeyboardType = .default, isDisable : Bool = false, autocapitalizationType :  UITextAutocapitalizationType = .sentences) {
        self.placeHolder  = placeHolder
      
        self.fontSize = fontSize
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.icon = icon
        self.iconColor = iconColor
        self.strokeColor = strokeColor
        self.cornorRaduis = cornorRaduis
        self.keyBoardType = keyBoardType
        self.isDisable = isDisable
        self.autocapitalizationType = autocapitalizationType
    }
    var body: some View {
        HStack {
            if let imageIcon = icon{
                Image(systemName: imageIcon).foregroundColor(iconColor)
            }
            Text(placeHolder).disabled(isDisable).autocapitalization(autocapitalizationType).keyboardType(keyBoardType)
            
        }.font(Font.system(size: fontSize))
            .padding()
            .background(RoundedRectangle(cornerRadius: cornorRaduis).fill(backgroundColor))
            .foregroundColor(textColor)
            .overlay(RoundedRectangle(cornerRadius: cornorRaduis).stroke(strokeColor))
    }
}

