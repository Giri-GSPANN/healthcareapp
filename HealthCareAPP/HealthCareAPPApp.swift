//
//  HealthCareAPPApp.swift
//  HealthCareAPP
//
//  Created by Giri Thangellapally on 17/11/22.
//

import SwiftUI

@main
struct HealthCareAPPApp: App {
    var body: some Scene {
        WindowGroup {
           SplashView()
        }
    }
}
